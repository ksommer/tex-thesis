TEX = xelatex -shell-escape
GENBIB = biber
GENGLS = makeglossaries
GENIDX = xindy -M page-ranges -M texindy -C utf8 -L german-din
VIEWER = gnome-open

MASTER := $(shell egrep -l '^[^%]*\\begin\{document\}' *.tex)
SRC = $(MASTER:%.tex=%)
PDF = $(MASTER:%.tex=%.pdf)

default: clean all

help:
	@echo '~~~~~~~~~~'
	@echo 'Masterdokument: $(MASTER)'
	@echo '~~~~~~~~~~'
	@echo 'make        | Führt "make clean" und "make all" nacheinander aus.'
	@echo 'make all    | Erstellt das komplette PDF ($(PDF)), inklusive Literaurverzeichnis und Glossar.'
	@echo 'make bib    | Erstellt die notwendigen Dateien für das Literaturverzeichnis.'
	@echo 'make pdf    | Erstellt nur (einmal) das PDF ($(PDF)). Falls Fehler auftreten führe "make bib" und/oder "make pdf" erneut aus.'
	@echo 'make gls    | Erstellt die notwendigen Dateien für das Glossar.'
	@echo 'make idx    | Erstellt die notwendigen Dateien für den Index.'
	@echo 'make view   | Öffnet das PDF des Masterdokuments ($(PDF)) im Standardbetrachter.'
	@echo 'make clean  | Säubert das Arbeitsverzeichnis von temporären Dateien.'
	@echo 'make help   | Zeigt diese Hilfe an.'

all: pdf bib gls pdf_thrice

bib:
	$(GENBIB) $(SRC)
	
pdf:
	$(TEX) $(SRC)

pdf_thrice:
	$(TEX) $(SRC)
	$(TEX) $(SRC)
	$(TEX) $(SRC)

gls:
	$(GENGLS) $(SRC)

idx:
	$(GENIDX) $(SRC).idx

view:
	$(VIEWER) $(PDF)

clean:
	find . -type f \( -iname '*.aux' -o -iname '*.log' -o -iname '*.idx' -o -iname '*.lof' -o -iname '*.lol' -o -iname '*.out' -o -iname '*.toc' -o -iname '*.lot' -o -iname '*.nlo' -o -iname '*.bbl' -o -iname '*.blg' -o -iname '*.brf' -o -iname '*.ilg' -o -iname '*.ind' -o -iname '*.nls' -o -iname '*.glo' -o -iname '*.tdo' -o -iname '*.dvi' -o -iname '*.ist' -o -iname '*.glg' -o -iname '*.gls' -o -iname '*.xdy' -o -iname '*.bcf' -o -iname '*.run.xml' -o -iname '*.*~' \) -exec rm {} \;
