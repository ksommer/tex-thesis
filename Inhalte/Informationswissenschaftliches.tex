\chapter{Informationswissenschaftliche und -tech\-no\-lo\-gische Grund\-lagen}
\label{ch:infowiss}
Die Idee des dieser Arbeit zugrundeliegenden Vorhabens besteht in der Untersuchung der Möglichkeiten für die digitale Abbildung von in der realen Welt existierenden Kulturgütern und ist eine klassische Aufgabe der Informationsverarbeitung, wie das folgende Zitat von Heiner Stuckenschmidt bestätigt:
\begin{quote}
„Die grundlegenden Aufgaben und Probleme der Informationsverarbeitung sind: […] bestimmte Ausschnitte der realen Welt in eine geeignete Darstellungsform zu überführen, die mit Hilfe des Computers manipuliert werden kann, um mit diesem Veränderungen in der realen Welt abzubilden.“\citep[5]{Stuckenschmidt200905}
\end{quote}

Dieses Kapitel zeigt welche Ansätze und Konzepte für obiges Vorhaben existieren und wie sie funktionieren. Ebenso wird eine Bewertung dieser durchgeführt und eine Entscheidung für die SemRes-Adaption getroffen.

\section{Evolution: WWW → Semantic Web}
\label{sec:evolution}
\begin{quote}
„The Semantic Web is not a separate Web but an extension of the current one, in which information is given well-defined meaning, better enabling computers and people to work in cooperation. The first steps in weaving the Semantic Web into the structure of the existing Web are already under way. […] For the semantic web to function, computers must have access to structured collections of information and sets of inference rules that they can use to conduct automated reasoning.“\autocite{BernersLee2001}
\end{quote}
Niemand anderes als Tim Berners-Lee selbst, der ‚Erfinder‘ des \gls{WWW}, schrieb obiges Zitat als eine realistische Einschätzung der Zukunft im Artikel „The Semantic Web“ im Jahr 2001. Jedoch bereits 1994 hatte er eine ganz konkrete Vorstellung von der Zukunft des Web, wie die beiden folgenden Grafiken und die beiden Zitate aus seiner Präsentation auf der ersten „World-Wide Web“-Konferenz in Genf, auf der auch die Gründung des \gls{W3C} bekanntgegeben wurde, zeigen.

\begin{quote}
„To a computer, then, the web is a flat, boring world devoid of meaning.“\autocite{BernersLee1994}
\end{quote}
\begin{figure}[H]
 \centering
    \includegraphics[max width=0.7\textwidth]{130929-BernersLee-weblevel.jpeg}
    \caption{Flat World (\tabcite{Datei:weblevel})}
 \label{fig:berneslee1}
\end{figure}

\begin{quote}
„Adding semantics to the web involves two things: allowing documents which have information in machine-readable forms, and allowing links to be created with relationship values. Only when we have this extra level of semantics will we be able to use computer power to help us exploit the information to a greater extent than our own reading.“\autocite{BernersLee1994}
\end{quote}
\begin{figure}[H]
 \centering
    \includegraphics[max width=0.6\textwidth]{130929-BernersLee-semantic.jpeg}
    \caption{Links with Relationship (\tabcite{Datei:semantic})}
 \label{fig:bernerslee2}
\end{figure}

\begin{quote}
„We expect the developments, methodologies, challenges, and techniques we’ve discussed here to not only give rise to a Semantic Web but also contribute to a new Web Science—a science that seeks to develop, deploy, and understand distributed information systems, systems of humans and machines, operating on a global scale.“\autocite{BernersLee2006}
\end{quote}
Dieses letzte Zitat entstammt ebenfalls einem Artikel, den Tim Berners-Lee 2006 zusammen mit Nigel Shadbolt und Wendy Hall veröffentlichte: „The Semantic Web Revisited“.

Auch wenn all diese Zitate über einen Zeitraum von mehr als zehn Jahre verteilt werden können, so lässt sich deren Quintessenz doch auf einen gemeinsamen Nenner bringen: sie beschreiben grundlegend die selbe Idee, das selbe Konzept, die selbe Vision.

Berners-Lee und viele andere Wegbereiter\_innen haben die Vision, dass aus „dummen“ Webseiten, die zwar von Menschen und Maschinen lesbar, deren Inhalte jedoch in der Regel nur von Menschen in ihrer gesamtheitlichen Bedeutung erfasst werden können, Daten werden, deren Semantik ebenfalls von Maschinen interpretiert werden können. Ausgangspunkt hierfür war das „Web 1.0“, welches die Grundlagen des Web, wie wir es heute kennen, geschaffen hat; es wurde jedoch hauptsächlich für den Transport von statischen HTML-Seiten benutzt. Deswegen bezeichnet man dieses Web auch als „Document Web“, wie auch die beiden Abbildungen aus dem Jahr 1994 deutlich veranschaulichen. Mit dem Beginn des „Web 2.0“ – welches in diesem Jahr zehn Jahre alt geworden ist – wurden die vorher etablierten Standards für den Datenaustausch und die Datenverarbeitung interaktiver, es wurde quasi zum „Mitmach-Web“. Ein gewisser Nachteil dieser Entwicklung lässt sich jedoch klar ausmachen: durch den rasanten Anstieg der Web-Inhalte und deren eigentlich fast unveränderte technische Repräsentation wuchs die Schwierigkeit des Information-Retrievals und besonders der Erschließung der Web-Dokumente.

Diese Grundproblematik ist allerdings weniger von der Menge der Informationen abhängig, als von der text- und wortbasierten Suche. Denn nur mit dieser ist es heutzutage größtenteils für Such\textit{maschinen} möglich, relevante Ergebnisse zu Suchanfragen zu liefern. Auch Standardisierungen für die Auszeichnung von Web-Dokumenten mit Metadaten, wie es beispielsweise \gls{DC} der „Dublin Core Metadata Initiative“ standardisiert hat, sind nur ganz kleine Schritte zur Beseitigung dieses Problems.

Eine Möglichkeit – vielleicht die einzige –, diesem Dilemma zu entgehen, ist die Auszeichnung der konkreten Daten selbst mit Eindeutigkeiten. Das Netz dieser Daten wird auch als \gls{LODC} bezeichnet (eine sehr anschauliche LODC ist in Abbildung \ref{fig:lodc} zu sehen) und ist ein Teil des Semantic Web. Die dafür zur Verfügung stehenden Technologien werden im Folgekapitel kurz vorgestellt.

\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{110919-lod-cloud-colored}
    \caption{„Linked Open Data Cloud (colored)“ (\tabcite{Datei:LODCcolored})}
 \label{fig:lodc}
\end{figure}

\minisec{Syntax und Semantik}
Auf Grundlage der folgenden Aussage von Pellegrini und Blumauer, die auf die lange Tradition der Bedeutungslehre hindeutet, würde es zu weit führen, den Begriff der Semantik an dieser Stelle ausführlich zu beleuchten:
\begin{quote}
„Es scheint also, als hätte Tim Berners-Lee […] die Büchse der Pandora geöffnet, da die Auseinandersetzung mit Semantik im engeren Sinn jedenfalls seit über 100 Jahren geführt wird und die Bedeutungslehre weit über 2000 Jahre alt ist.“\autocite[10]{PellegriniBlumauer200605} 
\end{quote}
Da sich das Folgekapitel mit semantischen Technologien beschäftigt, soll jedoch an dieser Stelle kurz auf die Definition der Begriffe „Syntax“ und „Semantik“ von Hitzler u.a. hingewiesen werden: „Allgemein steht […] Syntax für die normative \textit{Struktur} von Daten, welche erst durch Semantik eine \textit{Bedeutung} erhält.“\autocite[13]{HitzlerKrötzschRudolphSure2008}. In diesem Sinne werden sie auch in dieser Arbeit verstanden und verwendet.

\section{Semantische Technologien}
Ein Großteil der für die Umsetzung des Semantic Web benötigten Technologien werden heute im Alltag teilweise unbewusst genutzt. Sie haben jedoch eine lange Entwicklungs- und Erprobungszeit hinter sich.

\minisec{RDF}
Das \gls{RDF} ist ein vom W3C entwickeltes Datenmodell, mit dessen Hilfe sich die grundlegende Idee der Relation zwischen Daten-Objekten, sogenannten Resourcen, abbilden lässt. Die Relationen werden dabei als Tripel von \textit{(Subjekt, Prädikat, Objekt)} modelliert.

\minisec{URI}
Eine RDF-Resource ist in jedem Fall eindeutig mit einem \gls{URI} bezeichnet. Subjekte und Prädikate sind immer Resourcen, Objekte können Resourcen oder Literale (also Text) sein. Ein URI kann auch eine \gls{URL} sein; dies sollte zutreffen, wenn Daten als \gls{LOD} in der LODC verfügbar sein sollen. 

Das einfache Beispiel in Abbildung \ref{fig:rdftriple} zeigt diese beiden Regeln sehr deutlich anhand des \textit{Subjekts} „\texttt{http://de.wikipedia.org/wiki/Resource\_Description\_Framework}“ (also eine Resource mit einer URI/URL), welches zwei \textit{Prädikate} (URIs/URLs in roter Schrift) besitzt. Die \textit{Objekte} beider Tripel sind jeweils Literale.

\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{130929-WP-RDFGraph.png}
    \caption{RDF-Beispiel-Triple (\tabcite{Datei:Servlet164433})}
 \label{fig:rdftriple}
\end{figure}

Die syntaktische Auszeichnung von RDF-Daten kann mit unterschiedlicher Syntax erfolgen. Am weitesten verbreitet ist „RDF/XML“. Kürzere Repräsentationsformen sind „Notation 3“ (N3) und \gls{Turtle}, welches auch im weiteren Verlauf dieser Arbeit verwendet wird.

Im Folgenden ist zur besseren Veranschaulichung der unterschiedlichen Repräsentationen das oben grafisch präsentierte RDF-Beispiel in RDF/XML- und Turtle-Syntax zu sehen.

\singlespacing
\lstinputlisting[language=rdf, label=lst:rdf-example, caption=RDF-Beispiel in RDF/XML]{Code/rdf_example.rdf}
\onehalfspacing

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-example, caption=RDF-Beispiel in Turtle]{Code/rdf_example.ttl}
\onehalfspacing


Dass somit ein parallel und mit dem Web existierendes Datennetz geschaffen werden kann, zeigt das folgende Zitat von Dean Allemang u. a.:
\begin{quote}
„The main idea of the Semantic Web is to support a distributed Web at the level of data rather than at the level of presentation. Instead of having one webpage point to another, one data item can point to another, using global references called Uniform Resource Identifiers (URIs). […] The data model that the Semantic Web infrastructure uses to represent this distributed web of data is called the Resource Description Framework (RDF) […].“\autocite[7]{AllemangHendler200806}
\end{quote}

\minisec{RDFS und OWL}
Die gezeigte Auszeichnung und Repräsentation der Daten mittels RDF reicht aber noch nicht aus, um das Semantic Web zu einem Netz von semantisch verbundenen Datensätzen zu machen. Dafür fehlt das dahinterstehende Konzept.

Dieses Konzept, das zur Modellierung von Wissensbeständen notwendig ist, wird Ontologie genannt. Mit ihm lassen sich komplexe Relationen innerhalb und zwischen Konzepten abbilden. Eine eindeutigere Definition stammt von Tom Gruber aus dem Jahr 2009:
\begin{quote}
„In the context of computer and information sciences, an ontology defines a set of representational primitives with which to model a domain of knowledge or discourse.  The representational primitives are typically classes (or sets), attributes (or properties), and relationships (or relations among class members).  The definitions of the representational primitives include information about their meaning and constraints on their logically consistent application.“\autocite{Gruber2009}
\end{quote}

Auch für die Beschreibung von Ontologien gibt es geeignete (W3C-)Standards: \gls{RDFS} und \gls{OWL}. Dabei ist OWL noch einmal in die drei Sprachebenen OWL-Lite, OWL-DL und OWL-Full unterteilt. Der Unterschied zwischen diesen Vokabularen ist ihre Mächtigkeit. Je mächtiger das Vokabular ist, desto mehr Ausdrucksmöglichkeiten stehen für die Modellierung zur Verfügung.
 
Abbildung \ref{fig:onionFull} verdeutlicht die nach oben hin ansteigende Mächtigkeit der Vokabulare. Dabei ist anzumerken, dass jede Schicht alle Ausdrucksmöglichkeiten der unteren Schichten inkludiert.
 
\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{onionFull.pdf}
    \caption{Schemaübersicht (Zwiebelmodell) (\tabcite{Datei:onionFull})}
 \label{fig:onionFull}
\end{figure}

\minisec{Semantic Web}
Dieses Kapitel abschließend soll noch die folgende Abbildung des „Semantic Web Stack“ veranschaulichen, wie das Semantic Web aufgebaut ist. Die wichtigsten Teile davon wurden zuvor bereits beschrieben.
\begin{figure}[H]
 \centering
    \includegraphics[max width=0.6\textwidth]{SemWebStack-tbl-2006a.png}
    \caption{„Semantic Web Stack 2006“ (\tabcite{Datei:SemWebStack})}
 \label{fig:SemWebStack}
\end{figure}


\section{Vorhandene Konzepte zur Repräsentation restauratorischer Daten}
Nachdem nun ein paar Grundlagen geschaffen wurden, werden in diesem Unterkapitel Konzepte vorgestellt, die sich als geeignet zur Erfüllung der Aufgabenstellung dieser Arbeit erwiesen haben.

Vorab kann schon darauf hingewiesen werden, dass es leider kein Schema gibt, welches speziell für die Restaurierung konzipiert wurde. Wie in der Einleitung bereits beschrieben, gibt es jedoch glücklicherweise Ähnlichkeiten mit Einrichtungen, die sich ebenfalls um das kulturelle Erbe bemühen. Dies sind zum einen Museen, beispielsweise bezüglich der Verwaltung von Artefakten, und zum anderen Bibliotheken oder Archive, wenn es um die Verwaltung der Dokumentationen geht. Somit gab es also einige Anhaltspunkte, die bei der Suche nach einem geeigneten Konzept hilfreich waren.

\subsection{„ESE“ und „EDM“} 
Die Europäische Union hat im Rahmen des „Europeana.eu“-Projekts\footnote{Vgl. \url{http://europeana.eu}} – eine virtuelle Bibliothek – das auf RDF basierende \gls{EDM}. Dieses entstand aus dem \gls{ESE} (2008) und wurde 2011 offiziell als Ablösung von ESE, allerdings mit voller ESE-Kompatibilität, eingeführt. Aufgrund dessen wird im Folgenden nur noch auf EDM eingegangen.

Ziel der Europeana ist es, eine Plattform zu sein, in der aggregierte digitale Objekte zur Verfügung gestellt werden. Diese befinden sich jedoch nicht auf den Servern des Europeana-Projekts, sondern bei den jeweiligen Einrichtungen\footnote{Liste der teilnehmenden Einrichtungen: \url{http://europeana.eu/portal/europeana-providers.html}}. Nur die Metainformationen und Thumbnails werden vom Projekt bereitgestellt. Zur Zeit sind so schon über 2,4 Millionen Texte, Video, Bilder und Audiodateien\autocite[43]{Hyvoenen201210} über das Portal und in der LODC als RDF und XML frei zugänglich.

EDM basiert auf RDF und war, wegen seiner großen Verbreitung und recht guten Präsenz, zu Beginn dieser Arbeit das favoritisierte Schema. Schon alleine die Vorstellung, dass die FHP ein Provider der Europeana werden könnte und somit auch SemRes-Objekte dort findbar wären, waren eine große Motivation.

Leider stellte sich diese Vorstellung vorerst als nicht verwirklichbar heraus, denn bei EDM handelt es sich eher um ein Mapping-Schema für die in den teilnehmenden Institutionen verwendeten Metadatenschemata. Somit ist das EDM-eigene Vokabular sehr überschaubar und stellt nur einige wenige Klassen und Eigenschaften zur Verfügung (siehe Abbildungen \ref{fig:edmklassen} und \ref{fig:edmeigenschaften}). Mit diesen waren nach einigen Versuchen die SemRes-Daten nicht abbildbar.

\begin{figure}[H]
 \centering
    \includegraphics[width=0.75\textwidth]{130929-edmklassen.jpg}
    \caption{EDM-Klassen (\tabcite{Datei:edmklassen})}
 \label{fig:edmklassen}
\end{figure}

\begin{figure}[H]
 \centering
    \includegraphics[width=0.65\textwidth]{130929-edmeigenschaften.jpg}
    \caption{EDM-Eigenschaften (\tabcite{Datei:edmeigenschaften})}
 \label{fig:edmeigenschaften}
\end{figure}

\subsection{„CIDOC CRM“}
\label{sec:crm}
\begin{quote}
„The idea here [CIDOC] is that different metadata models used for memory organizations for different kinds of collection objects could be transformed into a form conforming to a more foundational semantic ontology model. The model serves as an intellectual guide for format creation, a language for analysis, and a data transportation format.“\autocite[44]{Hyvoenen201210}
\end{quote}
\begin{quote}
„CIDOC CRM 'provides definitions and a formal structure for describing the implicit and explicit concepts and relationships used in cultural heritage documentation'\footnote{\url{http://cidoc.ics.forth.gr/}}. This includes not only representation of objects but also their potentially complex provenance information.“\autocite[44]{Hyvoenen201210}
\end{quote}
Das \gls{CIDOC}, hat ein \gls{CRM} entwickelt, welches auch 2006 als ISO Standard 21127 verabschiedet und speziell als Ontologie für den Bereich des kulturellen Erbes geschaffen wurde. Bereits 1994 wurde mit der Entwicklung eines „Entity-Relationship-Model“ begonnen, welche jedoch 1996 zugunsten eines objektorientierten Modells umgestellt wurde. Im Jahr 1999 wurde dann das erste CRM veröffentlicht und bis heute immer weiter standardisiert. 2006 wurde eine erste offiziell vom \gls{ICOM} entwickelte Erweiterung des CRM für den bibliothekarischen Bereich vorgestellt: \gls{FRBRoo}. Dieses objektorientierte Modell wurde in Anlehnung an das CIDOC CRM auf Basis des von der \gls{IFLA} entwickelten \gls{FRBR} konzipiert.

Diese langjährigen Bestrebungen und Entwicklungen, sowie die Intention zur Entwicklung des Schemas sprachen grundlegend dafür, sich intensiver mit dem CRM für den SemRes-Einsatz auseinanderzusetzen. Schaut man sich außerdem die in Abbildung \ref{fig:crmklassen} gezeigten Hauptklassen des Schemas an, sieht man, dass die Konzeptualisierung sehr nah an den Bedarf im SemRes-Projekt herankommt.

\begin{figure}[H]
 \centering
    \includegraphics[width=1\textwidth]{crmklassen.png}
    \caption{CIDOC-CRM-Klassen (aus Hyvönen [2012])}
 \label{fig:crmklassen}
\end{figure}

Ein weiterer, sicher nicht zu unterschätzender Punkt für die Überlegung der Adaption ist die Frage nach der aktuellen (Weiter-)Entwicklung eines Schemas. Die Antwort darauf kann für das CRM positiv beantwortet werden. Durch die aktive Entwicklung von Erweiterungen, der Etablierung in großen Projekten (siehe unten), die Anhängigkeit an das ICOM/CIDOC und regelmäßige Veranstaltungen\footnote{Vgl. \url{http://www.cidoc-crm.org/special_interest_meetings.html}}durch die Arbeitsgruppe \gls{CRM-SIG} kann durchaus davon ausgegangen werden, dass die Entscheidung für das CRM auch nachhaltig ist.

Abbildung \ref{fig:crmklassen} verdeutlicht gut eine interessante konzeptionelle Überlegung: die CRM-Ereignisse (\texttt{E5\_Event}). Das heißt, dass Metadaten, bspw. das Datum der Herstellung eines Artefakts, nicht direkt mit dem modellierten Artefakt in Relation gesetzt werden\\
\hspace*{10mm}\texttt{<Artefakt> wurde\_hergestellt\_am <Datum>}\\
sondern die Beziehung wird über ein Ereignis, nämlich das der Herstellung des Artefakts, modelliert. Das heißt, dass die Relation letztlich als\\
\hspace*{10mm}\texttt{<Artefakt> wurde\_hergestellt\_durch <Herstellungs-Ereignis>} und\\
\hspace*{10mm}\texttt{<Herstellungs-Ereignis> fand\_statt\_am <Datum>}\\
abgebildet wird. Auf weitere Einzelheiten der Klassen und Eigenschaften wird an dieser Stelle nicht weiter eingegangen, da sich eine recht ausführliche Dokumentation der für SemRes relevanten Elemente im Kapitel „Modellierung“ ab Seite \pageref{ch:modellierung} befindet.

Allerdings werden noch kurz drei ausgewählte Institutionen und Projekte, die das CRM benutzen, vorgestellt. Eine Liste aller anwendenden Einrichtungen ist unter \url{http://www.cidoc-crm.org/uses_applications.html} zu finden.
\begin{itemize}
    \item Das „CLAROS“-Projekt\footnote{\url{http://www.clarosnet.org/}} bietet unter \url{http://data.clarosnet.org} einen LOD-Service mit den im CRM beschriebenen Daten in unterschiedlichsten Formaten an. Ein Beispiel für die verschiedenen Aufbereitungen und die Modellierung mit dem CRM ist das Foto „Wilhelm Dörpfeld im Kuppelgrab C“, welches aus dem Bestand von „Arachne“\footnote{\url{http://arachne.uni-koeln.de/}} stammt: die HTML-Ansicht bei Arachne ist unter {\footnotesize \url{http://arachne.uni-koeln.de/item/objekt/168330}} und die aufbereitete RDF-Ansicht bei CLAROS unter {\footnotesize \url{http://data.clarosnet.org/doc:arachne/entity/1161145}} einsehbar.
    \item Ein sehr großes Projekt, in dem gerade ein Museum an die LODC angebunden wird, ist das „British Museum“ (BM). Hier sind am Beispiel des „The Rosetta Stone“ die aufbereiteten RDF-Daten unter {\footnotesize \url{http://collection.britishmuseum.org/description/object/YCA62958.html}} und die HTML-Ansicht unter {\footnotesize \url{http://www.britishmuseum.org/research/collection_online/collection_object_details.aspx?objectId=117631&partId=1}} zu sehen. Bei der Umsetzung wird das BM vom „ResearchSpace“-Projekt\footnote{\url{http://www.researchspace.org/}} (RS) der Andrew W. Mellon Foundation\footnote{\url{http://www.mellon.org/}} unterstützt.
    \item Ein ebenfalls recht aktuelles Projekt ist „ConservationSpace“\footnote{\url{http://conservationspace.org/}} (CS). Wie der Name vermuten lässt, handelt es sich dabei um ein Projekt mit Bezug zur Restaurierung. Auch CS ist, genau wie RS, ein Programm der Andrew W. Mellon Foundation.
\end{itemize}

\minisec{„Erlangen CRM“}
Es existiert, zusätzlich zur offiziellen RDF-Version, die in OWL-DL 1.0 implementierte Version des CRM: \gls{ECRM}. Diese wird von Bernhard Schiemann, Martin Oischinger und Günther Görz am Institut für Informatik der Friedrich-Alexander-Universität in Erlangen-Nürnberg entwickelt\footnote{Vgl. \url{http://erlangen-crm.org/}}. Hintergrund für diese Implementierung waren vor allem die der offiziellen RDF-Version fehlenden Inferenzen\autocite{Goerz2008}.

Es existieren zwei Namensräume, die zur Benutzung des ECRM verwendet werden können. Ein versionierter Namensraum, der für die aktuelle Implementierung des CRM v5.0.4 \texttt{http://erlangen-crm.org/120111/} lautet und ein nicht versionierter Namensraum mit der URL \texttt{http://erlangen-crm.org/current/}. Auf der Webseite\footnote{\url{http://erlangen-crm.org/current-version}} der aktuellsten ECRM-Version sind Hinweise für die empfohlene Verwendung zu finden.

Nach einigen Experimenten und kleineren Fehlstarts beim Adaptieren bestehender Schemata und dem Versuch, ein eigenes Schema für SemRes zu entwickeln, wurde schließlich der praktische Teil dieser Arbeit, die Aufbereitung der restauratorischen Daten der SemRes-Anwendung zur Verarbeitung im Semantic Web, mittels des ECRM erfolgreich durchgeführt, wie das folgende Kapitel zeigt.

Es gilt jedoch zu beachten, dass im weiteren Verlauf keine Unterscheidung zwischen dem CRM und dem ECRM mehr gemacht wird. Wenn vom CRM die Rede ist, ist damit immer die OWL-DL-Implementierung, also das ECRM, gemeint.

