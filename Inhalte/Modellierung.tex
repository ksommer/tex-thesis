\chapter{Modellierung}
\label{ch:modellierung}
Bevor in den folgenden Unterkapiteln die konkreten Artefakt- und Dokumentationsmetadaten des „Löwen“ modelliert werden, sollen an dieser Stelle nur die benutzten Namensräume gezeigt werden.

Listing \ref{lst:ttl-head} zeigt, dass zusätzlich zur Benutzung des Vokabulars des Erlangen-CRM, welches mit dem Kürzel {\color{DarkSkyBlue}\lstinline+crm:+} den empfohlenen\autocite[Vgl.][„Note:“]{ECRM:Current} versionierten Namensraum einbindet, nur noch die grundlegenden Vokabulare RDF und RDFS verwendet werden.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-head, caption=RDF – Namensräume, lastline=3]{Code/loewe-komplett.final.ttl}
\onehalfspacing

\section{Das Artefakt}
In dem Listing \ref{lst:ttl-artefact-1} ist das erste SemRes-Artefakt/-Objekt mit der Definition ({\color{DarkSkyBlue}\lstinline+rdf:type+}) als CRM-Objekt {\color{DarkChameleon}\lstinline+E84_Information_Carrier+} und dem Bezeichner ({\color{DarkSkyBlue}\lstinline+rdfs:label+}) „Löwe“ als Literal. Dabei wird zum einen „\texttt{a}“ als gängige Turtle-Abkürzung\autocite[Vgl.][Kapitel 2.4]{W3C:Turtle} der \gls{IRI} für „\texttt{http://www.w3.org/1999/02/22-rdf-syntax-ns\#type}“ und zum anderen der Sprach-Tag „\texttt{@de}“ der Turtle-Syntax\autocite[Vgl.][Kapitel 2.5.1]{W3C:Turtle} zur Auszeichnung als deutschsprachiges Literal benutzt. 

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-artefact-1, caption=RDF – Artefakt „Löwe“, firstline=5]{Code/loewe-artefakt-1.ttl}
\onehalfspacing

\section{Material eines Artefakts}
\label{sec:material}
Während des schon in Kapitel \ref{sec:interflex} erwähnten Seminars wurde von der sogenannten Strukturierungsgruppe auch eine Klassifikation für die in der Restaurierung verwendeten Materialien, aus denen Artefakte bestehen können, erstellt. Diese umfasst insgesamt 539 Einträge über Materialien, die in die vier Gruppen Naturstein, Kunststein, Gips und Fassung eingeordnet sind.

Jetzt könnte man ja – wenn man Kenntnisse über die Bemühungen der Klassifikation in den Informationswissenschaften oder Einblicke in die Petrografie (Felsenkunde) hat – meinen, dass diese aufwändige Klassifizierungsarbeit nicht der Mühe wert war. Schließlich gibt es diese Materialien schon seit Anbeginn der Erdentstehung und seitdem sollte sich doch schon einmal jemand die Arbeit gemacht haben, diese zu klassifizieren. – Nun, dem ist auch so. Allerdings liegt der Teufel wieder einmal im Detail, wie das folgende Zitat\autocite[V]{Reinsch1991} von Dietmar Reinsch aus dem Vorwort seines Werkes \citetitles{Reinsch1991} zeigt.
\begin{quote}
„Miteinander reden und sich dabei verstehen, kann man offensichtlich in deutlich verschiedenen Sprachen […].\\
Es gibt auch den anderen Fall: Man spricht in der gleichen Sprache miteinander und versteht sich überhaupt nicht – was sehr verschiedene Ursachen haben kann; u.a. auch verschiedene inhaltliche Bedeutungen von Wörtern.\\
{[…]} Geowissenschaftlich ausgebildete Gesteinskundler, die Petrographen und Petrologen, und die technisch ausgebildeten Baustofftechniker sprechen äußerlich mit einer sehr ähnlichen Sprache – doch die allgemeine Denkweise in den Berufen und die Inhalte der Fachwörter differieren z.T. sehr handgreiflich. […] Dies führt in der Praxis […] im Zusammenhang mit immer komplexerer moderner Technik häufiger als eigentlich notwendig zu teuren Mißverständnissen […].”
\end{quote}

Dass diese Umstände auch (so oder ähnlich) auf die Denkmalpflege – also auch die Restaurator\_innen – zutrifft, zeigt schon der Titel des Werkes. Deshalb ist es also auch aus diesem Grund notwendig standardisiertes Vokabular für den vergleichsweise überschaubaren Bereich der Steinrestaurierung an der FHP zu schaffen. Die Hintergründe zu den verwendeten Quellen und Referenzen seitens der Strukturierungsgruppe, die zur Erstellung der Materialklassifikation benutzt wurden, sind leider nicht bekannt.

Der für das Artefakt „Löwe“ relevante Ausschnitt aus der von den Studierenden der Restaurierung erstellten Klassifikation der Materialien ist in Abbildung \ref{fig:materials-terrakotta} zu sehen.
\begin{figure}[H]
\centering
\tikzstyle{every node}=[draw=Aluminium5,thick,anchor=west]
\tikzstyle{selected}=[draw=DarkOrange,fill=LightOrange]
\begin{tikzpicture}[%
    font=\sffamily\small,
    grow via three points={one child at (0.5,-0.7) and
    two children at (0.5,-0.7) and (0.5,-1.4)},
    edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
\node {Materialien}
child { node {{\scriptsize […]}}}
child { node {\textbf{3} Kunststein}
    child { node {\textbf{3.1} mineralisch}
        child { node {{\scriptsize […]}}}
        child { node [selected] {\textbf{3.1.8} Terrakotta}}
        child { node {{\scriptsize […]}}}
    }
    child [missing] {}
    child [missing] {}
    child [missing] {}
    child { node {{\scriptsize […]}}}
}
child [missing] {}
child [missing] {}
child [missing] {}
child [missing] {}
child [missing] {}
child { node {{\scriptsize […]}}};
\end{tikzpicture}
\caption{Ausschnitt aus der Materialklassifikation}
\label{fig:materials-terrakotta}
\end{figure}

Um nun dem Objekt das klassifizierte Material „Terrakotta“, aus dem das Artefakt besteht, zuzuordnen, wird dieses um die CRM-Eigenschaft {\color{ScarletRed}\lstinline+P45_consists_of+}, dessen Objekt ein weiterer Node ist, erweitert.
\newpage
\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-material-1, caption=RDF – Artefakt „Löwe“ – Material 1, firstline=5]{Code/loewe-material-1.ttl}
\onehalfspacing

Die Definition des klassifizierten Materials „Terrakotta“ selbst wird durch die CRM-Klasse {\color{DarkChameleon}\lstinline+E57_Material+} bewerkstelligt. Der Name des Materials wird mit {\color{DarkSkyBlue}\lstinline+rdf:label+} als deutschsprachiges Literal angegeben. Weiterhin wird, um eine bessere Identifikation zu gewährleisten, das Material um das Prädikat {\color{ScarletRed}\lstinline+P3_has_note+} mit dem Text der Klassifikationsnummer und dem Namen erweitert.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-material-2, caption=RDF – Artefakt „Löwe“ – Material 2, firstline=5]{Code/loewe-material-2.ttl}
\onehalfspacing

Wie in Abbildung \ref{fig:materials-terrakotta} ersichtlich, müssen – der Vollständigkeit halber – ebenso die beiden Oberbegriffe „Kunststein“ und „mineralisch“ definiert werden. Außerdem kann so ebenfalls gezeigt werden, wie der hierarchische Aufbau der Klassifikation umgesetzt werden kann.

Für die Abbildung von Hyperonymen stellt das CRM das Prädikat {\color{ScarletRed}\lstinline+P127_has_broader_term+} bereit. Das inverse Prädikat {\color{ScarletRed}\lstinline+P127i_has_narrower_term+} ist für die Definition von Hyponymen vorgesehen. Laut dem Kommentar von {\color{ScarletRed}\lstinline+P127_has_broader_term+} in der CRM-Dokumenation(„It allows Types to be organised into hierarchies. This is the sense of "broader term generic (BTG)" as defined in ISO 2788“\citep[105]{CIDOC:CRM504.pdf}), empfiehlt das CRM den Aufbau einer Hierarchie von unten nach oben und bezieht sich dafür auf den ISO-Standard 2788 aus dem Jahre 1986, der den Titel „Documentation -- Guidelines for the establishment and development of monolingual thesauri“\citep{ISO:2788} trägt und in einer überarbeiteten Version mit der Nummer „ISO 25964-1:2011“ verfügbar ist. In Absprache mit dem Erstgutachter wurde hier für die Modellierung der Hierarchie von oben nach unten und somit die Verwendung des Präfixes {\color{ScarletRed}\lstinline+P127i_has_narrower_term+} entschieden, wie Listing \ref{lst:ttl-material-3} zeigt.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-material-3, caption=RDF – Artefakt „Löwe“ – Material 3, firstline=5]{Code/loewe-material-3.ttl}
\onehalfspacing

In Abbildung \ref{fig:loewe-material-3} ist die modellierte Hierarchie gut zu erkennen.
\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{Code/loewe-material-3.pdf}
    \caption{Graph – Artefakt „Löwe“ – Material 3}
 \label{fig:loewe-material-3}
\end{figure}

\section{Entstehungszeit eines Artefakts}
Die Entstehungszeit des Artefakt wird mittels der CRM-Eigenschaft {\color{ScarletRed}\lstinline+P108i_was_produced_by+} und eines weiteren Nodes (ein CRM-Ereignis) abgebildet.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-entstehung-1, caption=RDF – Artefakt „Löwe“ – Entstehung 1, firstline=5]{Code/loewe-entstehung-1.ttl}
\onehalfspacing

{\color{Chocolate}\lstinline+http://semres.fh-potsdam.de/database/temporal/1+} ist ein CRM-Ereignis der Klasse {\color{DarkChameleon}\lstinline+E12_Production+} und wird nun dazu benutzt, die Entstehung (inkl. der Entstehungszeit) abzubilden. Die CRM-Eigenschaft {\color{ScarletRed}\lstinline+P82_at_some_time_within+} dient zur Darstellung der Entstehungszeit und der Node ist anhand der CRM-Klasse {\color{DarkChameleon}\lstinline+E61_Time_Primitive+} definiert.
\newpage
\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-entstehung-2, caption=RDF – Artefakt „Löwe“ – Entstehung 2, firstline=5]{Code/loewe-entstehung-2.ttl}
\onehalfspacing

Die Relation zwischen dem CRM-Ereignis der Herstellung und der konkreten Entstehungszeit zeigt der Graph in Abbildung \ref{fig:loewe-entstehung-2}.
\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{Code/loewe-entstehung-2.pdf}
    \caption{Graph – Artefakt „Löwe“ – Entstehung 2}
 \label{fig:loewe-entstehung-2}
\end{figure}

Des Weiteren wird in dem CRM-Ereignis mittels der CRM-Eigenschaft {\color{ScarletRed}\lstinline+P14_carried_out_by+} die Person – oder wie im Beispiel des Artefakts „Löwe“ die Körperschaft – definiert, die das Artefakt (bei der Entstehung) erschaffen hat.
\newpage
\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-entstehung-3, caption=RDF – Artefakt „Löwe“ – Entstehung 3, firstline=5]{Code/loewe-entstehung-3.ttl}
\onehalfspacing

Zur Abbildung der bereits erwähnten Körperschaft „Manufaktur für Stuck und Terrakotta F. Koch“ wird die CRM-Klasse {\color{DarkChameleon}\lstinline+E74_Group+} benutzt. Weiterhin wird für die Instanzierung des konkreten Namens {\color{DarkChameleon}\lstinline+E82_Actor_Appellation+} verwendet, welche durch das CRM-Prädikat {\color{ScarletRed}\lstinline+P131_is_identified_by+} geschieht.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-entstehung-4, caption=RDF – Artefakt „Löwe“ – Entstehung 4, firstline=5]{Code/loewe-entstehung-4.ttl}
\onehalfspacing

Der Graph in Abbildung \ref{fig:loewe-entstehung-4} veranschaulicht die Relationen aus Listing \ref{lst:ttl-entstehung-4} noch einmal.

\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{Code/loewe-entstehung-4.pdf}
    \caption{Graph – Artefakt „Löwe“ – Entstehung 4}
 \label{fig:loewe-entstehung-4}
\end{figure}

\section{Restaurierungszeiträume eines Artefakts}
Wie eingangs beschrieben wird eine Restaurierungsphase mit Hilfe einer Dokumentation beschrieben und belegt; ohne diese Dokumentation gibt es keine Restaurierungsphasen.\\
Diese Phase ist erneut ein CRM-Ereignis und wird mittels der CRM-Eigenschaft {\color{ScarletRed}\lstinline+P31i_was_modified_by+} und einen weiteren Node abgebildet.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-restaurierung-1, caption=RDF – Artefakt „Löwe“ – Restaurierung 1, firstline=5]{Code/loewe-restaurierung-1.ttl}
\onehalfspacing

Das CRM-Event mit der URI {\color{Chocolate}\lstinline+http://semres.fh-potsdam.de/database/temporal/3+} ist nicht wie bei dem Entstehungsereignis die CRM-Klasse {\color{Chameleon}\lstinline+E12_Production+}, sondern eine Modifikation ({\color{Chameleon}\lstinline+E11_Modification+}). Auch in dieser wird die Zeit anhand des Prädikats {\color{ScarletRed}\lstinline+P82_at_some_time_within+} abgebildet.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-restaurierung-2, caption=RDF – Artefakt „Löwe“ – Restaurierung 2, firstline=5]{Code/loewe-restaurierung-2.ttl}
\onehalfspacing

Analog zum CRM-Ereignis der Herstellung zeigt der Graph in der folgenden Abbildung \ref{fig:loewe-restaurierung-4} die Relation zwischen dem CRM-Ereignis der Restaurierung und der konkreten Zeit der Restaurierung.
\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{Code/loewe-restaurierung-2.pdf}
    \caption{Graph – Artefakt „Löwe“ – Restaurierung 2}
 \label{fig:loewe-restaurierung-2}
\end{figure}

\section{Dokumentation der Restaurierungsphase}
Die restauratorische Dokumentation kann unter Verwendung des CRM-Prädikats {\color{ScarletRed}\lstinline+P70i_is_documented_in+} und eines Nodes des CRM-Typs {\color{Chameleon}\lstinline+E31_Document+} modelliert werden. Dieser Node besitzt dann erstens das Prädikat {\color{ScarletRed}\lstinline+P72_has_language+} zur Festlegung, dass es sich um ein deutschsprachiges Schriftstück oder Dokument handelt, und zweitens das Prädikat {\color{ScarletRed}\lstinline+P3_has_note+}, um den langen deutschsprachigen Titel der Dokumentation zuzuweisen.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-restaurierung-3, caption=RDF – Artefakt „Löwe“ – Restaurierung 3, firstline=5]{Code/loewe-restaurierung-3.ttl}
\onehalfspacing

Abbildung \ref{fig:loewe-restaurierung-3} zeigt die Relation zwischen dem CRM-Ereignis und der Dokumentation grafisch.
\begin{figure}[H]
 \centering
    \adjustimage{trim=0mm 0mm 160pt 0mm,clip,max width=1\textwidth}{Code/loewe-restaurierung-3.pdf}
    \caption{Graph – Artefakt „Löwe“ – Restaurierung 3}
 \label{fig:loewe-restaurierung-3}
\end{figure}

Zusätzlich zu den bereits modellierten Metadaten der Dokumentation müssen die Personen, die die Restaurierung durchgeführt haben und die in der restauratorischen Dokumentation angegeben sind, abgebildet werden (dies kann auch eine einzelne Person sein). Diese Relation kann durch das CRM-Prädikat {\color{ScarletRed}\lstinline+P14_carried_out_by+} und einen neuen Node der CRM-Klasse {\color{Chameleon}\lstinline+E39_Actor+} im Ereignis der Restaurierungsphase modelliert werden.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-restaurierung-4, caption=RDF – Artefakt „Löwe“ – Restaurierung 4, firstline=5]{Code/loewe-restaurierung-4.ttl}
\onehalfspacing

Wie Listing \ref{lst:ttl-restaurierung-4} und dessen grafische Veranschaulichung in Abbildung \ref{fig:loewe-restaurierung-4} zeigen, wird der neue Node {\color{Chocolate}\lstinline+http://semres.fh-potsdam.de/database/persistent/8+} wiederum über das Prädikat {\color{ScarletRed}\lstinline+is_identified_by+} mit einem neuen Node der CRM-Klasse {\color{Chameleon}\lstinline+E82_Actor_Appellation+} verbunden. Dieses Vorgehen sollte bereits aus der Modellierung (siehe Listing \ref{lst:ttl-entstehung-4} und Abbildung \ref{fig:loewe-entstehung-4}) bekannt sein.
\begin{figure}[H]
 \centering
    \includegraphics[max width=1\textwidth]{Code/loewe-restaurierung-4.pdf}
    \caption{Graph – Artefakt „Löwe“ – Restaurierung 4}
 \label{fig:loewe-restaurierung-4}
\end{figure}

Der Vollständigkeit halber sollen in den Listings \ref{lst:ttl-restaurierung-5} und \ref{lst:ttl-restaurierung-6} auch noch die beiden anderen beteiligten Restauratorinnen, die an der Restaurierungsphase laut Tabelle \ref{tab:documentations} beteiligt waren, modelliert werden.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-restaurierung-5, caption=RDF – Artefakt „Löwe“ – Restaurierung 5, firstline=5]{Code/loewe-restaurierung-5.ttl}
\onehalfspacing

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-restaurierung-6, caption=RDF – Artefakt „Löwe“ – Restaurierung 6, firstline=5]{Code/loewe-restaurierung-6.ttl}
\onehalfspacing

Der komplett modellierte Datensatz befindet sich aus Platzgründen im Anhang. – Unter \ref{lst:anhang-ttl} mit Turtle-Syntax und unter \ref{lst:anhang-rdf} in RDF/XML-Syntax.\\
Weiterhin ist im Anhang \ref{sec:graph_loewe_komplett} der vollständige Graph zu finden.

\section{Schwierigkeiten und Herausforderungen}
„Viele Wege führen nach Rom …“ – Diese Redewendung steht immer ganz am Anfang eines sehr verschlungenen Weges bei der geplanten systematischen – und manchmal auch \textit{un}systematischen – Umsetzung großer Ziele; anders war es auch bei dem vorliegenden Projekt nicht. Dennoch war es nicht ganz so aussichtslos, wie der XKCD-Comic es beschreibt – auch wenn Analogien durchaus zu erkennen sind.
\begin{figure}[H]
    \centering
    \includegraphics[max width=0.5\textwidth]{xkcd-good_code.png}
    \caption{XKCD – Good Code (\tabcite{Datei:XKCD})}
    \label{fig:xkcd}
\end{figure}

Eigentlich schien die Aufgabe des Umsetzens der vereinbarten Metadaten der Artefakte und Dokumentationen in RDF relativ leicht und unkompliziert zu sein. Auch wenn viele grundlegende Dinge bezüglich der theoretischen Grundlagen, die sich auch in Kapitel \ref{ch:infowiss} wiederfinden, aufgearbeitet und auch neu gelernt werden konnten, so machte sich die fehlende Erfahrung mit der Entwicklung eines eigenen Schemas, oder der Adaption bestehender Konzepte auf eine ähnliche Domäne doch bemerkbar. Zwar halfen in dieser Situation die in Kapitel \ref{sec:konventionen} selbst definierten und vereinbarten Konventionen etwas weiter. Allerdings bleibt selbst nach der weitgehend erfolgreichen Modellierung immer noch relativ offen, ob diese in der realen Implementierung letztlich sinnvoll sind und wenn ja, in welchem Ausmaß.

Für die beiden ausgewählten Themenbereiche der Verwendung der Prädikate und der konzeptuellen Modellierung sollen im Folgenden kurz ein paar Beispiele für die Schwierigkeiten und Herausforderungen gegeben werden.

\subsection{Prädikate}
Eine Schwierigkeit, die auch bei den Überlegungen zur Konzeption auftrat, war die Frage nach den allgemein zu verwendenden Vokabularen. Bereits in der Einleitung zu diesem Kapitel wurde gezeigt, dass bei der durchgeführten Modellierung nur die Vokabulare CRM, RDF und RDFS benutzt wurden. Diese Entscheidung wurde getroffen, um die RDF-Daten (wie in der Konvention 1. auf Seite \pageref{anchor:konv1} festgelegt) so einfach wie möglich auszuzeichnen. Es hätte auch die Möglichkeit bestanden weitere Vokabulare zu benutzen, wie beispielsweise das weit verbreitete \gls{DC}.

Speziell bei der durchaus berechtigten Überlegung zur Benutzung von DC traten jedoch die ersten Mehrdeutigkeiten auf. Würde die ein Artfakt erschaffende Person oder Körperschaft mittels {\color{DarkSkyBlue}\lstinline+dc:creator+} oder eher mit {\color{DarkSkyBlue}\lstinline+dc:maker+} ausgezeichnet?\\
Da eine eindeutige und schlüssige Beantwortung dieser Beispielfrage nicht gefunden werden konnte, wurde sich gegen die Benutzung von DC entschieden.

Eine nächste ‚Baustelle‘ tat sich bei der so einfach klingenden Konvention 7. bezüglich der Bezeichner (siehe Seite \pageref{anchor:konv7}) auf. Zur Auswahl standen dem ersten Anschein nach {\color{DarkSkyBlue}\lstinline+rdfs:label+}, {\color{DarkSkyBlue}\lstinline+rdfs:value+}, {\color{ScarletRed}\lstinline+crm:P1_is_identified_by+} (für generische Bezeichner), {\color{ScarletRed}\lstinline+crm:P3_has_note+}, {\color{ScarletRed}\lstinline+crm:P48_has_preferred_identifier+}, {\color{ScarletRed}\lstinline+crm:P78_is_identified_by+} (\textit{Time-Span}), {\color{ScarletRed}\lstinline+crm:P87_is_identified_by+} (\textit{Place Appellation}), {\color{ScarletRed}\lstinline+crm:P102_has_title+}, {\color{ScarletRed}\lstinline+crm:P131_is_identified_by+} (\textit{Actor Appellation}) und {\color{ScarletRed}\lstinline+crm:P149_is_identified_by+} (\textit{Conceptual Object}). Bei der zusätzlich angedachten Verwendung von DC hätten dann beispielsweise auch noch {\color{DarkSkyBlue}\lstinline+dc:title+} und {\color{DarkSkyBlue}\lstinline+dc:description+} in die Entscheidung mit einbezogen werden müssen.\\
Leider wurde die Verwirrung durch die verschiedenen Möglichkeiten und Konzepte nicht gänzlich behoben. Geklärt wurde jedoch die Verwendung der im CRM konzipierten Prädikate {\color{ScarletRed}\lstinline+P???_is_identified_by+}, die jeweils im Range einer Klasse {\color{DarkChameleon}\lstinline+EXX_Appellation+} sind. Auch wurde durch eine etwas ausführlichere Erklärung\footnote{Vgl. \url{http://lists.ics.forth.gr/pipermail/crm-sig/2013-September/002001.html0}} seitens Martin Dörr vom „\textit{Institute of Computer Science}“ der „\textit{Foundation for Research and Technology - Hellas}“ auf der CRM-SIG-Mailingliste im September 2013 die konzeptionelle Verwendung einiger aufgezählter Prädikate klarer. Schlussendlich fiel die Entscheidung jedoch auf die einfache und einheitliche Verwendung von {\color{DarkSkyBlue}\lstinline+rdfs:label+} und den CRM-Prädikaten {\color{ScarletRed}\lstinline+P???_is_identified_by+}.

\subsection{Konzeption}
Wie bereits im vorstehenden Unterkapitel angedeutet, trafen die Überlegungen zur Verwendung unterschiedlicher Vokabulare auch bei der Kozeption der SemRes-RDF-Daten zu.

Eine weitere Herausforderung, die letztlich in die Festlegung 8. auf Seite \pageref{anchor:konv8} mündete, bestand im Verstehen des Zusammenspiels zwischen der Funktionsweise des CRM-Schemas und der Verwendung inverser Prädikate. Dies soll an einem kleinen Beispiel veranschaulicht werden.

Wie bei der Modellierung der Materialien im vorangegangenen Kapitel \ref{sec:material} schon beschrieben, wird die hierarchische Abbildung der Materialienklassifikation durch das CRM-Prädikat {\color{ScarletRed}\lstinline+P127i_has_narrower_term+} bei der Auszeichnung von Hyponymen erreicht. Nun könnte logisch daraus folgen, dass das inverse Prädikat {\color{ScarletRed}\lstinline+P127_has_broader_term+} zusätzlich in allen Hyperonymen verwendent wird. Denn somit wäre klar und deutlich in allen Materialien ausgedrückt, welche Ober- und Unterbegriffe sie besitzen.

Allerdings schafft dies zum einen einen gewissen Overhead, da bei einer Materialkategorie (wie beispielsweise Naturstein) sehr viele Materialien direkt untergeordnet sind. Dass diese Daten jedoch maschinell erstellt und verarbeitet würden, entkräftigt dieses Argument in gewisser Weise. Zum anderen ist die Verwendung von {\color{ScarletRed}\lstinline+P127i_has_narrower_term+} eher geeignet, da jedes Material nur eine übergeordnete Materialkategorie besitzt.\\
Eine sehr viel interessante Begründung für die Konvention 8. sind jedoch die in das CRM-Schema ‚eingebauten‘ Inferenzregeln bei der Deklaration der Prädikate. Dadurch ist es mit Hilfe eines sogenannten \textit{Semantic Reasoner} möglich, diese Regeln zu nutzen.

Abbildung \ref{fig:inferenz1} zeigt den Ausschnitt eines Screenshots von Protégé. Es wurden nur der RDF-Datensatz des Artefakts „Löwe“ geladen und das Material „Kunststein: mineralisch“ ausgewählt. Als einzige Objekt-Eigenschaft ist unten rechts {\color{ScarletRed}\lstinline+P127i_has_narrower_term+} zu sehen.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{130929-inferenz1.png}
    \caption{Screenshot – Protégé – Material ohne Inferenz}
    \label{fig:inferenz1}
\end{figure}

Nach dem Starten des Reasoner wird, wie Abbildung \ref{fig:inferenz2} zeigt, das Material automatisch mit dem RDF-Objekt „Kunststein“ über das inverse Präfix {\color{ScarletRed}\lstinline+P127_has_broader_term+} verbunden.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{130929-inferenz2.png}
    \caption{Screenshot – Protégé – Material mit Inferenz}
    \label{fig:inferenz2}
\end{figure}
Somit ist gezeigt worden, dass die zwar sehr einfach klingende Konvention 8. einen recht komplexen Hintergrund besitzt, letztlich aber auf einer sinnvollen und nachvollziehbaren Entscheidung basiert.

