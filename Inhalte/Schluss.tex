\chapter{Schlussbetrachtungen}
\label{ch:schluss}
Zu Beginn dieses abschließenden Teils wird kurz auf die akuelle Implementierung der im vorangegangen Kapitel dokumentierten RDF-Daten innerhalb des SemRes-Projekts eingegangen. Weiterhin wird ein Ausblick auf mögliche Anreicherungen der SemRes-RDF-Daten durch andere LODC-Daten gegeben und es wird ein Fazit bezüglich der Thematik und der Erfüllung der Zielsetzung dieser Arbeit gezogen.

\section{Implementierung}
Der im Kapitel „\nameref{ch:modellierung}“ produzierte RDF-Datensatz für das Artefakt „Löwe“ und die dazugehörige Dokumentation „Konservierung und Restaurierung eines Löwen aus Terrakotta, Jägerallee 28/29 Potsdam“ sind online verfügbar. Alle URIs liefern den entsprechenden RDF-Datensatz aus und sind somit schon jetzt ein Teil der LODC.

Da die unter \url{http://semres.fh-potsdam.de} verfügbare Web-Anwendung, die alle Daten der Tabellen \ref{tab:artefacts} und \ref{tab:documentations} bereits in einer relationalen Datenbank vorhält, aktuell jedoch keinen \textit{Triplestore} bereitstellt, wurden die einzelnen RDF-Datensätze manuell für die Auslieferung erstellt. Nun, nachdem durch diese Arbeit eine Basis für die Bereitstellung der SemRes-Daten als RDF-Daten geschaffen wurde, kann die Firma „River Byte“ den nächsten Schritt zur Teilnahme des SemRes-Projekts an der Vision des Semantic Web angehen.

Je nach Implementierung des Triplestore besteht die Möglichkeit beim Aufruf einer SemRes-URI mittels \textit{Content Negotiation} entweder eine HTML-Seite anzuzeigen, oder die RDF-Daten auszuliefern, so wie es in der Praxis bereits umgesetzt wird und in dem Dokument „Best Practice Recipes for Publishing RDF Vocabularies“ der W3C-Arbeitsgruppe „\textit{Semantic Web Deployment Working Group}“ beschrieben ist:
\begin{quote}
„When an HTTP client attempts to dereference a URI, it can specify which type (or types) of content it would prefer to receive in response. It does this by including an 'Accept:' field in the header of the request message, the value of which gives MIME types corresponding to preferred content types.”\autocite{W3C:PublishRDF}
\end{quote}

Als Vorbild dafür könnte beispielsweise das „CLAROS“-Projekt, dessen Triplestore unter \url{http://data.clarosnet.org} erreichbar ist, dienen. Dort wird die URI \url{http://id.clarosnet.org/BSR/0010035}, die gleichzeitig eine Web-Resource ist, beim Öffnen in einem Web-Browser auf die URL \url{http://data.clarosnet.org/doc/BSR/0010035} weitergeleitet und erhält dort eine HTML-Ansicht der RDF-Daten, die aus einer SPARQL-Abfrage des Triplstores generiert wird. Im Gegensatz dazu ist es Programmen, die RDF direkt verarbeiten können, möglich die URI abzurufen und als Antwort RDF-Daten zu erhalten.

Weiterhin existiert auf der SemRes-Webseite bereits ein Demo, welches die Verknüpfung der Datenbank-Objekte visualisiert. In Abbildung \ref{gx:proto_viz} ist ein Screenshot dieses Demos zu sehen. In dieser ist recht gut zu sehen, wie die Artefakte (grün) mit den Dokumentationen (gelb) über das Prädikat „ist dokumentiert in“ in Relation stehen. Und da die Dokumentation über das Präfix „wurde ausgeführt von“ mit einer Person verbunden ist, stehen ebenso eine Person und ein Artefakt miteinander in Verbindung (wie ja auch der Graph zeigt).
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{prototyp-visualisierung.png}
	\caption{SemRes-Demo – Screenshot der Visualisierung (27.09.2013)}
	\label{gx:proto_viz}
\end{figure}

\section{\textit{The Missing Link} – Ausblick}
Ein ebenso wichtiger Teil der LODC, neben der semantischen Aufbereitung und dem Zurverfügungstellen der eigenen Daten in der LODC, ist die Integration von anderen oder fremden Daten der LODC. Der folgende Abschnitt soll an mehreren Beispielen aufzeigen, welche Optimierungen bei der Modellierung der SemRes-Daten zur Erfüllung des zweiten Teils nötig sind.

Eine erste Maßnahme besteht in der Verwendung konkreter Datentypen für entsprechende Daten, statt der einfachen Verwendung von Literalen – also beispielsweise der Definition eines Datums als Datum und nicht nur als Text.\\
Dazu bietet es sich an, auf vordefinierte und bereits anderweitig benutzte Standards zurückzugreifen. Dies ist zum Beispiel mit den in der \gls{XSD} vom \gls{W3C} Datentypen möglich.

Eine zweite Möglichkeit besteht in der Erweiterung der SemRes-Daten durch die Möglichkeit des Ausdrückens oder des Verwendens direkter Äquivalenzen zwischen RDF-Objekten in SemRes und anderen Datenbeständen innerhalb der LODC. Dies ist beispielsweise mittels des in OWL definierten Prädikats {\color{DarkSkyBlue}\lstinline+owl:sameAs+} durchführbar. Dadurch ist es dann möglich „Individuals, welche in unterschiedlichen Dokumenten definiert wurden, gleichzusetzen, als Teil der Vereinigung zweier Ontologien“\autocite{W3C:OWLGuideDE}.

Zusätzlich zu den drei im Folgenden näher erläuterten Beispielen für die Integration der XSD-Datentypen, der geografischen Datenbank „\textit{GeoNames}“ und der virtuellen Personennormdatei \gls{VIAF}, würde es sich beispielsweise auch anbieten, die SemRes-Daten mit den Polymeren aus der POLYKON-Datenbank\footnote{Vgl. \url{http://polykon.fh-potsdam.de}} zu verbinden. Dieses Projekt stellt seine Daten jedoch noch nicht als Linked Data zur Verfügung. Eine Verknüpfung mit diesen Inhalten würde jedoch die Qualität der SemRes-Daten erheblich verbessern. Allerdings wäre es auch dadurch möglich, nicht nur die Metadaten der Artefakte und Dokumentationen semantisch abzubilden, sondern auch Teile der Dokumentationsinhalte. Dies ist ganz sicher eine spannende Herausforderung.

\subsection{XSD-Datentypen}
Das folgende Beispiel, zeigt anhand des Restaurierungszeitraums des Artefakts die exemplarische Integration von Datentypen nach der W3C-Empfehlung XSD. Das Ziel dieser Empfehlung lautet wie folgt:
\begin{quote}
„The XML specification defines limited facilities for applying datatypes to document content […]. However, document authors […] often require a higher degree of type checking to ensure robustness in document understanding and data interchange.“\autocite{W3C:XSD}
\end{quote}

Zur Integration ist es zuerst notwendig, die XSD-Spezifikation mittels des neuen Namensraums {\color{DarkSkyBlue}\lstinline+xsd:+} zu integrieren, wie das Listing \ref{lst:ttl-xsd} in Zeile 4 zeigt. Zur Repräsentation der Datumsangaben, siehe die Zeilen 20 und 22 des Listings, wurde der Datentyp {\color{DarkSkyBlue}\lstinline+xsd:gYearMonth+} gewählt – es stehen seitens der XSD-Definition noch \texttt{dateTime}, \texttt{date}, \texttt{gYearMonth}, \texttt{gYear}, \texttt{gMonthDay}, \texttt{gDay} und \texttt{gMonth} als einfache Datentypen zur Repräsentation von Datumsangaben zur Verfügung. Mit diesem lassen sich Jahreszahlen und Monate nach dem gregorianischen Kalender\autocite[Vgl.][]{W3C:XSD} ausdrücken. Der \gls{IRI} für einen Datentyp wird dabei in Turtle mittels {\color{DarkSkyBlue}\lstinline+^^+} (in Unicode U+005E U+005E) angegeben\autocite[Vgl.][Kapitel 2.5.1]{W3C:Turtle}.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-xsd, caption=RDF – „Löwe“ – Erweiterung mit XSD-Datentypen]{Code/semres-xsd.ttl}
\onehalfspacing

Zu beachten ist, dass die Prädikate {\color{DarkSkyBlue}\lstinline+P82a_begin_of_the_begin+} und {\color{DarkSkyBlue}\lstinline+P82a_begin_of_the_begin+} leider noch nicht Teil von CIDOC CRM sind. Somit ist eigentlich auch die Verwendung des {\color{DarkSkyBlue}\lstinline+crm:+}-Namensraums für diese Eigenschaften nicht zulässig bzw. führt zu einer nicht ausreichend implementierten Auflösung, durch die fehlenden Prädikate unter der URI des CRM. Ihre Einführung wurde von Vladimir Alexiev, der bei der Firma Ontotext für das (schon in Kapitel \ref{sec:crm} vorgestellte) „ResearchSpace“-Projekt (RS) arbeitet, im November 2012 auf der Mailingliste der CRM-SIG mit dem Betreff „ADDITION: property "active in period"“ vorgeschlagen\footnote{Vgl. \url{http://lists.ics.forth.gr/pipermail/crm-sig/2012-November/001906.html}}. Beide Prädikate werden zur Zeit schon für RS verwendet. Weitere Hintergründe seines Vorschlags können auch in dem Dokument „How to implement CRM Time in RDF“\footnote{Vgl. \url{http://www.cidoc-crm.org/docs/How_to\%20implement\%20CRM_Time_in\%20RDF.pdf}}, welches die Schwierigkeiten von Datumsdefinitionen mit dem CRM näher erläutert und mögliche Lösungsansätze aufzeigt, nachgelesen werden.

Weitere ausgewählte XSD-Datentypen, die neben der Spezifizierung als Datumsangabe zur konkreteren Auszeichnung der SemRes-Daten verwendet werden können, sind:
\begin{itemize}
    \item \texttt{integer} für Maßangaben;
    \item \texttt{int} für ganzzahlige Angaben;
    \item \texttt{time} für Zeitangaben.
\end{itemize}

\subsection{GeoNames}
\begin{quote}
„Die GeoNames–Datenbank enthält über 8 Millionen geographische Namen, die über 6,5 Millionen topografischen Objekten entsprechen und in jeweils eine von neun Klassen und eine von über 650 Codes kategorisiert sind. Neben Ortsnamen in verschiedenen Sprachen sind auch Längen- und Breitengrad, Höhe über Meer, Einwohnerzahl, administrative Unterteilung und Postleitzahlen in der Datenbank enthalten.“\autocite{WP:GeoNames}
\end{quote}
Weiterhin sind die Daten als GeoNames-RDF-Objekte in der LODC verfügbar und somit bietet sich die Integration in die SemRes-Daten eindeutig an.

Die Definition des schon in der Einleitung zu diesem Unterkapitel beschriebenen Namensraums {\color{DarkSkyBlue}\lstinline+owl:+}, zeigt das Listing \ref{lst:ttl-geonames} in Zeile 3.

Zur Veranschaulichung der Äquivalenzauszeichnung mittels des Prädikats {\color{DarkSkyBlue}\lstinline+owl:sameAs+} wurde der RDF-Datensatz des Artefakts „Löwe“ um die Angabe erweitert, an welchem Ort die Bearbeitung {\color{DarkChameleon}\lstinline+E11_Modification+} stattfand.\\Hierfür wird das CRM-Prädikat {\color{ScarletRed}\lstinline+P7_took_place_at+} benutzt, welches auf den neuen Node {\color{Chocolate}\lstinline+<http://semres.fh-potsdam.de/database/place/1>+} verweist. Dieser ist mit der CRM-Klasse {\color{DarkChameleon}\lstinline+E53_Place+} definiert und wird mittels {\color{DarkSkyBlue}\lstinline+owl:sameAs+} als äquivalent zur GeoNames-Web-Resource {\color{Chocolate}\lstinline+http://www.geonames.org/2852458+} ausgezeichnet.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-geonames, caption=RDF – „Löwe“ – Erweiterung für Geonames]{Code/semres-geonames.ttl}
\onehalfspacing

Für die weiteren, im Folgenden aufgelisteten, ausgewählten Geo-Metadaten (Ortsangaben) von SemRes-Artefakten und SemRes-Dokumentationen ist die Benutzung der GeoNames-Daten vorstellbar und qualitätssteigernd:
\begin{itemize}
    \item Produktion/Herstellung des Artefakts;
    \item Entstehung der Dokumentation;
    \item Standort des Artefakts;
    \item Aufbewahrungsort der Dokumentation.
\end{itemize}
\newpage
\subsection{VIAF}
\begin{quote}
„Das Virtual International Authority File (VIAF) ist eine virtuelle internationale Normdatei für Personendaten. Es ist ein Gemeinschaftsprojekt mehrerer Nationalbibliotheken und Bibliotheksverbünde, das durch das Online Computer Library Center (OCLC) betrieben wird. Das Projekt wurde gemeinsam von der Deutschen Nationalbibliothek und der Library of Congress initiiert.“\autocite{WP:VIAF}
\end{quote}
Des Weiteren befinden sich in der VIAF Körperschaftsdaten und auch geografische Namensnormdaten. Ebenso, wie bei GeoNames, handelt es sich bei den VIAF-Daten um sehr weit verbreitete und qualitativ hochwertige (LOD-)Daten, was sie für ihren Einsatz im SemRes-Projekt auszeichnet.

Die Anreicherung der SemRes-Daten wird analog zur Implementierung von GeoNames durchgeführt. Zu beachten ist dabei allerdings, dass für das Beispiel nicht der bisher verwendete RDF-Datensatz des Artefakts „Löwe“ nutzbar ist. Da dieser keine Personen aus der VIAF-Personennormdatei enthält, wurde ein Pseudo-Artefakt, welches durch den Künstler „Michelangelo Buonarroti“ erschaffen wurde, benutzt.

Es wird dabei wie beim Artefakt „Löwe“ verfahren; ein CRM-Ereignis für die Herstellung des Artefakts ({\color{DarkChameleon}\lstinline+E12_Production+}) wird verwendet. In diesem wird mittels des Prädikats {\color{ScarletRed}\lstinline+P14_carried_out_by+} ein Node der CRM-Klasse {\color{DarkChameleon}\lstinline+E39_Actor+} angesprochen, welcher die Künstlerin oder den Künstler identifiziert. Innerhalb dieser Nodedefinition wird dann mit dem Prädikat {\color{DarkSkyBlue}\lstinline+owl:sameAs+} die Äquivalenz zu einer VIAF-RDF-Resource (am Beispiel der Person „Michelangelo Buonarroti“: {\color{Chocolate}\lstinline+http://viaf.org/viaf/24585191+}; siehe Zeile 19 im Listing \ref{lst:ttl-viaf}) ausgedrückt.

\singlespacing
\lstinputlisting[language=turtle, label=lst:ttl-viaf, caption=RDF – Pseudo-RDF-Datensatz – Erweiterung für VIAF]{Code/semres-viaf.ttl}
\onehalfspacing

Im Folgenden sind ausgewählte Personen- und Körperschaftsangaben zu SemRes-Artefakten und SemRes-Dokumentationen aufgelistet, für die die Benutzung der VIAF-Datensätze vorstellbar und qualitätssteigernd sind:
\begin{itemize}
    \item Auftraggeber\_in (auch Institution) der Dokumentation (Restaurierung);
    \item Auftraggeber\_in (auch Institution) des Artefakts (Herstellung);
    \item Eingentümer\_in/Rechtsträger\_in (auch Institution) des Artefakts;
    \item für das Artefakt zuständige Denkmalpfleger\_in(en)/Denkmalbehörde(n);
    \item an der Restaurierung/Dokumentation beteiligte Fachkräfte.
\end{itemize}

\section{Fazit}
Die Umsetzung im Modellierungsteil dieser Arbeit hat gezeigt, dass das CIDOC-CRM respektive das Erlangen CRM gut geeignet sind, die im theoretischen Teil definierten Anforderungen zu erfüllen und zu den ebenda festgelegten Zielen zu gelangen.

Damit wurde erreicht, dass die semantische Aufbereitung restauratorischer Daten auf recht innovative Weise geschehen kann und es dadurch, ganz im Sinne der Idee und Funktionsweise des Semantic Web, möglich ist, die SemRes-Daten als Linked Open Data der LOD-Cloud zur Verfügung zu stellen.

Für die SemRes-Daten selbst wurde bewerkstelligt, dass nun semantische Relationen zwischen Dokumentationen und den restaurierten Artefakten existieren. Dies ist so mit dem relationalen Datenbankmodell nur mit hohem Aufwand realisierbar. Ebenso verhält es sich mit den Beziehungen zwischen den Objekten.\\
Auf den Daten, die nun zur Verfügung gestellt werden können, sind auch neue Recherchen mit qualitativen Trefferlisten möglich, die von klassischen, Text vergleichenden, Suchmaschinen nur sehr schwer zu erstellen sind. Beispielsweise besteht nun die Option Suchanfragen zu stellen nach:
\begin{enumerate}
    \item allen Artefakten, die im 13. Jahrhundert entstanden sind;
    \item allen Artefakten aus 1., die im 19. Jahrhundert restauriert wurden;
    \item allen Artefakten aus 2., die von dem Künstler ABC erschaffen wurden;
    \item allen Dokumentationen zu den Restaurierungsphasen der Artefakte aus 1.
    \item […]
\end{enumerate}

Zusammenfassend kann also festgestellt werden, dass die semantische Anreicherung der restauratorischen Daten prototypisch erfolgreich war und der Mehrwert für die Restaurierung klar erkennbar ist. Der Ausblick hat schlussendlich auch noch gezeigt, dass in den vorgeschlagenen Erweiterungen einige Potenziale im weiteren Ausbau des SemRes-Projekts stecken. 

Es bleibt somit abschließend – und dabei auf die beiden recht inspirierenden Eingangszitate Bezug nehmend – nur noch eins zu sagen:

\vspace*{5mm}
\begingroup
\centering
{\Large„But sharing isn‘t immoral — it’s a moral imperative.“}
\begin{flushright}
\textit{Aaron Swartz}
\nocite{Swartz200807}
\end{flushright}
\endgroup

