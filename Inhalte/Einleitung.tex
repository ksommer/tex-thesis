\chapter{Einleitung}
\label{ch:einleitung}
Zu Beginn dieser Arbeit werden – als Einführung in die Thematik – Hintergrundinformationen bereitgestellt, die dabei helfen sollen, den Kontext besser zu veranschaulichen und somit das Verstehen der rezipierenden Personen zu unterstützen. Dazu wird eine gewisse Chronologie – Idee → Seminar → Thesis – bemüht, die ebenfalls die Entwicklung der Thematik veranschaulicht.

Es wird an dieser Stelle ausdrücklich darauf hingewiesen, dass diese Arbeit und deren Inhalte immer im Zusammenhang mit der \gls{FHP} zu betrachten sind. Dies gilt vor allem für dieses Kapitel, da die meisten beschriebenen Fakten so nur für die Restaurierung an der FHP, speziell in der Fachrichtung Stein, zutreffen und es eher nicht möglich ist, diese für den Bereich der Konservierung und Restaurierung zu verallgemeinern.\\
Weiterhin ist anzumerken, dass im Folgenden der Begriff „Restaurierung“ für „Konservierung und Restaurierung“ verwendet wird – im Gegensatz zum englischsprachigen Raum, in dem in der Regel „\textit{Conservation}“ benutzt wird.

\section{Die Idee – Restauratorischer Hintergrund}
\label{sec:idee}
Während des Studiums an der FHP sind im Studiengang „Restaurierung“ des Fachbereichs „Architektur und Städtebau“ viele praktische Übungen und Prüfungsleistungen zu erbringen. Dabei werden neben den praktischen und technischen Fertigkeiten vor allem wissenschaftlich theoretische und somit auch dokumentarische Kompetenzen vermittelt und erlernt. Dies bedeutet, dass sich die (angehenden) Restaurator\_innen unter anderem mit den Themengebieten Restaurierungs-, Natur- und Kunstwissenschaften, Kunstgeschichte, Materialkunde, Aufbau und Werktechnik sowie der Ikonographie des zu bearbeitenden restauratorischen Objekts – also restauratorische Systematiken bei der Untersuchung und Behandlung geschädigter oder veränderter Kunstwerke und Denkmäler – beschäftigen. Diese finden sich, als essenzieller Teil der Restaurierungsphase, abschließend auch in der zugehörigen Dokumentation wieder. Diese Auseinandersetzung stellt ein wichtiges und unverzichtbares Aufgabenfeld in der Restaurierung dar, da sie der Entscheidungsfindung für konservatorische und restauratorische Maßnahmen dient. Die in der Dokumentation enthaltenen Informationen über durchgeführte Restaurierungsmaßnahmen sind deshalb zum einen ein unermesslicher Wissensschatz dieser Disziplin. Zum anderen wird die eigentliche restauratorische Aufgabe ungemein erschwert, wenn diese Informationen nicht vorhanden sind, da es für die geplante Restaurierung notwendig ist zu wissen, aus welchem Material das Objekt genau besteht, welche Restaurierungsphasen bereits durchlaufen und welche Materialien dabei verwendet wurden – um nur einige Gründe zu nennen.

Um welche Art von Objekten handelt es sich denn aber nun eigentlich bei den erwähnten restauratorischen Objekten? Der Studiengang „Restaurierung“ beschreibt den Schwerpunkt seines Studiums selbst als die „Ausbildung zur Konservierung und Restaurierung auf dem Gebiet der Baudenkmalpflege [und diese] schließt aber auch den musealen Bereich mit ein“\citep[Vgl.][]{WWW.FHPRest}. Somit umfassen restauratorische Objekte Denkmäler, museale Objekte und allgemeine Kunstwerke. Um im weiteren Verlauf dieser Arbeit eine klare sprachliche Trennung zwischen restauratorischen Objekten und Objekten im informationswissenschaftlichen und technologischen Sinn zu erreichen, wird im weiteren Verlauf für ein restauratorisches Objekt der Begriff „Artefakt“ benutzt werden.

Denkmale, die laut dem Duden als „erhaltenes [Kunst]werk, das für eine frühere Kultur Zeugnis ablegt“\citep{Duden:Denkmal2013} definiert werden, können, im Falle dass an ihrer „Erhaltung ein institutionelles öffentliches Interesse besteht“\citep{WP:Denkmal}, unter Denkmalschutz gestellt werden – egal, ob sich die Artefakte in Privatbesitz oder in Besitz der öffentlichen Hand befinden. Für diese Artefakte sind dann Denkmalbehörden zuständig. Bedingt durch den deutschen Föderalismus, nach dem die so genannte „Kulturhoheit“ nich beim Bund, sondern bei den Bundesländern liegt, gibt es in Deutschland sechzehn für den Denkmalschutz zuständige Behörden, die wiederum jeweils individuelle Gesetzgebungen und Richtlinien haben. Alle Dokumentationen von und zu Artefakten im Land Brandenburg müssen beim „Brandenburgischen Landesamt für Denkmalpflege (und Archäologischen Landesmuseum)“ abgeliefert werden. Dort werden sie archiviert und stehen auf Nachfrage interessierten Nutzenden (vor allem also Restaurator\_innen) zur Verfügung.\\
 All diese Umstände zeigen, dass es um eine einheitliche Behandlung der Artefakte eher ungünstig bestellt ist und die Eingrenzug des Kontextes dieser Thematik auch auf die FHP unbedingt nötig ist.

Das heißt also, dass es sich bei den Artefakten um schützenswerte und unter behördlicher Aufsicht stehende Kulturgüter und somit um kulturelles Erbe handelt. Da die Artefakte eng mit den Dokumentationen der Restaurierungsphasen verknüpft sind und die Informationen in den Dokumentationen essenziell sind, bedarf es eines durchdachten und nachhaltigen Umganges mit beiden Dingen.

\section{Das Seminar – „SemRes“}
\label{sec:interflex}
Im Wintersemester 2012/2013 fand in der \gls{FHP} das interdisziplinäre Seminar „IX 5-3 – Semantische Dokumentation in der Restaurierung“ in den Fachbereichen \gls{FB5} und \gls{FB2} statt. Unter der Leitung von Herrn Professor Ernesto De Luca aus dem FB5 und Herrn Professor Peter Kozub aus dem FB2, der Leiter der Steinwerkstatt war, nahmen Studierende aus den Bachelor-Studiengängen „Information und Dokumentation B.A.“ (FB5) und „Restaurierung“ (FB2) sowie aus dem Master-Studiengang „Informationswissenschaften M.A.“ (FB5) teil und setzten das Credo von InterFlex „Förderung von Interdisziplinarität und Flexibilität zur Integration von Forschung, Wissens- und Technologietransfer in die grundständige Lehre“\citep[Vgl.][]{interflex.incom.org-about} um.

Ausgangspunkt für diese InterFlex-Veranstaltung war die Tatsache, dass für die im Rahmen des restauratorischen Studiums an der FHP anfallenden Dokumentationen in der Fachrichtung Stein bisher keine geeigneten digitalen Verwaltungs- und Ablagesysteme existieren. Dieser Umstand führt dazu, dass eine Nachnutzung zum einen erschwert und zum anderen oft nicht vollständig durchführbar ist. Weiterhin stehen so für die Restaurierung unabdingbare Informationen nicht in geeigneter Weise zur Verfügung (vergleiche Kapitel \ref{sec:idee}).

Ausgehend von der Planung der beiden Lehrenden ergaben sich die folgenden Arbeitspakete für das unter dem Arbeitstitel „SemRes“ – ein Silbenkurzwort bestehend aus „Semantik“ und „Restaurierung“ – geplante und veranstaltete Projekt:
\begin{enumerate}
    \item Verbesserung des Zugangs zu den restauratorischen Dokumentationen;
    \item Erstellung eines semantischen Konzepts;
    \item Visualisierung der Relationen zwischen den Objekten;
    \item Personalisierung (optional).
\end{enumerate}
Ein weiteres Ziel bestand in der Entwicklung einer prototypischen Anwendung, in der die Ergebnisse der Arbeitspakete abgebildet sind. Zum Erreichen dieses Ziels wurde eine Kooperation mit der Firma „River Byte“ eingegangen, die vornehmlich für die direkte technische Umsetzung der studentischen Arbeitsergebnisse verantwortlich war. Diese Kooperation war außerdem notwendig, da zum einen die technische Infrastruktur innerhalb der FHP nicht für ein solches Projekt geeignet ist und da zum anderen somit die Anforderungen an die technischen Fähigkeiten der Studierenden auf dem kurrikularen Niveau gehalten werden konnten.

Während des Seminars wurden letztlich jedoch nicht alle oben aufgeführten Arbeitspakete bearbeitet. Speziell die beiden Punkte bzgl. des semantischen Konzepts und der Abbildung der Relationen wurden während der Laufzeit des Seminars nicht realisiert. Hauptgrund dafür war, dass aus strategischen Gründen im Seminar entschieden wurde, vor allem einen Fokus auf die Strukturierung der Dokumentation und die Konzeption der Web-Anwendung zu legen, um so einen (durchaus notwendigen und nützlichen) Prototypen, der auf einem relationalen Datenbanksystem basiert, für die weiteren Schritte und Arbeitspakete zu schaffen. Mit diesem Prototypen war die grundlegende Ablage und Verwaltung digitaler restauratorischer Dokumentationen bereits möglich, es fehlten jedoch die semantischen Ansätze und Ideen. Die anderen Arbeitspakte wurden dafür größtenteils sehr akribisch und sorgsam bearbeitet. Dadurch wurde bspw. auch die sogenannte „Standarddokumentation“ des FB2 einer intensiven Prüfung seitens der Arbeitsgruppe „Strukturierung“ unterzogen und mehrere Klassifikationen, z.B. auch für Materialien (siehe Kapitel \ref{sec:material}), wurden geschaffen. Somit stellen die Arbeiten der Studierenden definitiv wichtige Grundlagen des Projekts und dieser Arbeit dar.

\label{anchor:semres-domain} Die schon angesprochene Web-Anwendung ist als Subdomain der FHP-Hauptdomain unter der Adresse \url{http://semres.fh-potsdam.de} erreichbar – auch hier stand der Arbeitstitel Pate.

\subsection{Umzusetzende Metadaten}
Wie bereits beschrieben handelt es sich bei den zu bearbeitenden Daten – für die Verwaltung in der Datenbank und die semantische Bereitstellung – um echte Dokumentationen von Restaurierungsmaßnahmen an der FHP. Um diese etwas zu konkretisieren befindet sich zum einen im Anhang \ref{sec:loewe_deckblatt} das Deckblatt einer Dokumentation und zum anderen als Anhang \ref{sec:loewe_projektdatenblatt} das zugehörige und in der Dokumentation enthaltene sogenannte „Projektdatenblatt“. Dieses Projektdatenblatt enthält alle relevanten und wichtigen Metainformationen zum behandelten Artefakt und der vorliegenden Dokumentation. Die Einführung eines solchen Projektdatenblatts für alle Dokumentationen an der FHP war auch Ziel der Bestrebung zur Erstellung einer Standarddokumentation.

In den folgenden zwei Tabellen findet sich zusammengefasst eine Auswahl aus fünf Projektdatenblättern entnommenen Metadaten, die es grundlegend semantisch zu modellieren galt. In Tabelle \ref{tab:artefacts} sind alle Artefakte aufgelistet.

\begin{table}[H]
\footnotesize
\rowcolors{1}{Aluminium1}{white}
\caption{Restaurierte Objekte/Artefakte}
\label{tab:artefacts}
\begin{tabularx}{\textwidth}{lXXXX}
\toprule
\textbf{lfd. Nr.} & \textbf{Titel} & \textbf{Künstler\_in} & \textbf{Material} & \textbf{Entstehung} \\
\midrule
\midrule
1 & Löwe & Manufaktur für Stuck und Terrakotta F. Koch & Terrakotta & Ende 19. Jh. \\
2 & Grabmal „Baumstamm“ & R. Riese & Elbsandstein - Varietät Cottaer & Ende des 19. Jh. – Anfang 20. Jh. \\
3 & Denkmal des August Friedrich Eisenhart (1774-1846) & L. Bronzel & Büste: Elbsandstein - Varietät Cottaer, Sockel: Schlesicher Sandstein & 1900 – 1940 \\
4 & Marmorgrabmal  Gertrud Janke & Paul Heisler, Franz Tübbecke & Carrara Marmor & ca. 1906 – ca. 1909 \\
5 & Marmor-Engel & vermutlich Bernhard Afinger & Marmor (Carrara Ordinario) & Vermutl. 2. Hälfte 19. Jh. \\
\bottomrule
\end{tabularx}
\end{table}

Die im Schema umzusetzenden Metadaten der Dokumentationen sind in Tabelle \ref{tab:documentations} dargestellt. Dabei entspricht eine Dokumentation immer einer Restaurierungsphase.
\begin{table}[H]
\footnotesize
\rowcolors{1}{Aluminium1}{white}
\caption{Dokumentationen}
\label{tab:documentations}
\begin{tabularx}{\textwidth}{lXXl}
\toprule
\textbf{Artefaktnr.} & \textbf{Titel} & \textbf{Restaurator\_in} & \textbf{Rest.zeitraum} \\
\midrule
\midrule
1 & Konservierung und Restaurierung eines Löwen aus Terrakotta, Jägerallee 28/29 Potsdam & Maria Kielholz, Luise Fusco, Jennifer Scollin & Okt 10 – Jul 12 \\
2 & Grabmal „Baumstamm“ & Pia Dölitscher, Franziska Müller, Markus Huschenbeth & Nov 06 – Jul 07 \\
3 & Dokumentation; Denkmal des August Friedrich Eisenhart in Potsdam & Sophie Hoepner, Robert Kannis, Oliver Kohn, Judith Teichmann & Okt 08 – Jul 09 \\
4 & Dokumentation; Konservierung und Restaurierung vom Marmorgrabmal „Gertrud Janke“; Südwestkirchhof Stahnsdorf & Niklas Underwood, Rosa J. Gottwald, Luise Fusco & Okt 11 – Feb 13 \\
5 & Restaurierung einer Marmorskulptur des Laasower Schlossparks; 2004/2005 & Franziska Müller, Gernot Püschel & Okt 04 – Jul 05 \\
\bottomrule
\end{tabularx}
\end{table}

\section{Fragestellungen der Master-Thesis}
\label{sec:thesis}
Bevor im Folgekapitel detaillierter auf die semantischen Technologien des Internets eingegangen wird, sollen an dieser Stelle die Fragestellungen, die dieser Arbeit zugrunde liegen, beleuchtet werden. Außerdem soll im Folgenden der zugrunde liegende Zweck der semantischen Abbildung restauratorischer Daten erläutert werden.

Wie bereits im vorangegangen Abschnitt \ref{sec:interflex} dargelegt, war ein Ziel des Seminars die restauratorischen Daten und Informationen semantisch abzubilden. Dieses Ziel neben den anderen zu erreichen war allerdings wie schon erwähnt etwas zu ambitioniert. Da der Autor dieser Master-Thesis jedoch intensiv in das SemRes-Seminar involviert war und sich durch seine 2012 verfasste Bachelor-Arbeit mit dem Titel „POLYKON: Webbasierte Datenbankanwendung – Nutzbarmachung wissenschaftlicher Daten aus dem Bereich Restaurierung“\autocite{Sommer201202} bereits in die informationswissenschaftlich-restauratorische Thematik eingearbeitet hatte, stellte dies eine sehr positive Ausgangssituation für eine erfolgreiche Bearbeitung des ausstehenden Ziels und den damit einhergehenden Abschluss des SemRes-Projekts dar.

Schon bei der Konzeption und der Planung der Projektidee stand fest, dass durch die Speicherung der restauratorischen Dokumentationen mittels der geplanten Web-Anwendung ein gewisser Mehrwert gegenüber dem vorhanden analogen Ablagesystem geschaffen würde. Bezüglich der höchst relevanten Informationen, die wie eingangs beschrieben essenziell für ein systematisches Vorgehen in der Restaurierung sind, würde jedoch das Speichern in einer Datenbank und das Ausliefern mittels (statischer) HTML-Seiten eher einer Einbahnstraße gleichkommen. Die dem \textit{Semantic Web} zugrundeliegende Idee des Teilens von maschinenlesbaren Daten auf der Bedeutungsebene und der daraus resultierenden Vision, aus unterschiedlichen Informationen – nicht Daten – Wissen zu generieren, wäre somit schon in gewisser Weise zu Beginn verhindert worden. Hinzu kommt auch, dass Semantic Web nicht nur ein Trend oder \textit{Buzzword} ist, sondern sich in dem letzten Jahrzehnt zu einer immer stabileren Version eines verbesserten Webs – des Web 3.0 – entwickelt hat. Sicher ist es noch weit davon entfernt, mehr als nur „42“ auf die Frage „nach dem Leben, dem Universum und dem ganzen Rest“ zu präsentieren. Die Bausteine jedoch, auf denen es basiert, sind wohldurchdacht, mittlerweile jahrelang erprobt und werden ständig weiterentwickelt. Es scheint also nur noch eine Frage der Zeit und der Anzahl der Protagonist\_innen – die sich trauen, auf den noch langsam rollenden Zug aufzuspringen – zu sein, bis semantische Technologien auch im Alltag den Durchbruch schaffen werden.

\minisec{Fragestellungen}
\label{sec:fragestellung}
Die Möglichkeit des Semantic Web direkt Resourcen per Hyperlinks – und nicht wie im bis dato zum Großteil etablierten und verwendeten Web ‚nur‘ Hypertexte, die im besten Fall nur über einige wenige textuelle Metadaten verfügen – zu verbinden soll auch im SemRes-Projekt genutzt werden. Dafür ist jedoch die Beantwortung einiger grundlegender Fragen nötig, der sich diese Arbeit – besonders den Fragen nach der Umsetzung – verschrieben hat.
\begin{itemize}
    \item Welche Mehrwerte stellen semantisch aufbereitete Informationen in der Restaurierung dar?
    \item Welche technischen Lösungsansätze existieren bereits und können diese gegebenenfalls genutzt werden?
    \item Welche Voraussetzungen müssen für SemRes gegeben sein?
    \item Wie können die Erkenntnisse praktisch angewendet werden?
\end{itemize}
Dies soll nun im Folgenden geschehen.

