\chapter{Konzeption}
\label{ch:konzeption}
Die Herausforderung dieser Arbeit bestand darin, im Vorfeld der Modellierung eine Reihe an Dingen zu bedenken und festzulegen, um letztlich die Funktionalität der Implementierung zu gewährleisten.\\
Dieser und der folgende Teil „\nameref{ch:modellierung}“ der Arbeit zeigen die konzeptionellen Überlegungen und die praktische Umsetzung der RDF-Modellierung am konkreten Beispiel des Artefakts „Löwe“ (siehe Tabelle \ref{tab:artefacts}) und der zugehörigen Dokumentation „Konservierung und Restaurierung eines Löwen aus Terrakotta, Jägerallee 28/29 Potsdam“ (siehe Tabelle \ref{tab:documentations}). Die Beschreibung einiger Schwierigkeiten und Herausforderungen schließt dieses Kapitel ab.

\minisec{Hinweis zu den Quelltextformatierungen:}
Für die Listings im RDF-Format wird \gls{Turtle} verwendet. Diese „Teilmenge des von Tim Berners-Lee und Dan Connolly's [sic!] entworfenen Notation 3 […] (N3)“\citep{WP:Turtle} weist, im Gegensatz zu RDF/XML, weniger Quelltext-\textit{Overhead} auf, was – für den Autor – zu einer einfacheren Lesbarkeit, speziell in den Listings in dieser Arbeit, führt. Außerdem werden folgende Farben zur besseren Übersichtlichkeit eingesetzt:
\begin{itemize}
    \item {\color{DarkSkyBlue}\lstinline+Subjekte+}, {\color{DarkSkyBlue}\lstinline+Objekte+} und {\color{DarkSkyBlue}\lstinline+Präfixe+} → {\color{DarkSkyBlue}blau}
    \item {\color{DarkChameleon}\lstinline+CRM-Klassen+} → {\color{DarkChameleon}grün}
    \item {\color{ScarletRed}\lstinline+CRM-Prädikate+} → {\color{ScarletRed}rot}
    \item {\lstinline+Literale+} und {\lstinline+Werte+} → schwarz
    \item {\color{Chocolate}\lstinline+URIs+} → {\color{Chocolate} hellbraun}
    \item {\color{Aluminium4}\lstinline+Kommentare+} → {\color{Aluminium4}grau}
\end{itemize}
Auslassungen, um den Quelltext für eine bessere Übersichtlichkeit der Listings etwas zu kürzen, sind mit {\color{Aluminium4}\lstinline+# [...]+} gekennzeichnet.

\minisec{Hinweis zu den Abbildungen:}
Die im Folgenden zur besseren Veranschaulichung der Sachverhalte generierten Graphen wurden mit \href{http://wifo5-03.informatik.uni-mannheim.de/bizer/rdfapi/}{RAP (RDF API for PHP)}, welches unter die \href{http://www.gnu.org/copyleft/lesser.txt}{GNU LESSER GENERAL PUBLIC LICENSE (LGPL)} gestellt ist, produziert. Außerdem wurde die \href{http://protege.stanford.edu/}{Protégé-Plattform}, die unter der \href{http://www.mozilla.org/MPL/1.1/}{Mozilla Public License Version 1.1} lizensiert ist, benutzt, um teilweise die Inferenzen darzustellen.

\minisec{Hinweis zur verwendeten Literatur:}
Um nachfolgend nicht an allen CRM-Klassen und CRM-Eigenschaften einen Verweis auf die Quelle zu deren Dokumentation unterbringen zu müssen, finden sich an dieser Stelle die grundlegenden Quellenangaben zur Dokumentation des CIDOC-CRM und des verwendeten Erlangen CRM.
\begin{itemize}
    \item „Definition of the CIDOC Conceptual Reference Model“\autocite{CIDOC:CRMHTML} (als HTML für die aktuelle und offizielle Version 5.0.4)
    \item „Definition of the CIDOC Conceptual Reference Model“\autocite{CIDOC:CRM504.pdf} (als PDF für die aktuelle und offizielle Version 5.0.4)
    \item OWLDoc-Dokumentation des ECRM 120111 (welches CIDOC-CRM 5.0.4 entspricht)\autocite{ECRM:OWLDoc}
    \item „CIDOC CRM 5.0.1 Graphical Representation“\autocite{Vladimir:CRMGraph} (wenn auch nicht ganz aktuell, dafür übersichtlich)
    \item „Definition des CIDOC Conceptual Reference Model“\autocite{CIDOC:DE2010} (wenn auch nicht ganz aktuell, dafür auf Deutsch)
\end{itemize}

\section{Allgemeine Konventionen der Modellierung}
\label{sec:konventionen}
Da der Autor selbst im Umgang mit CIDOC-CRM und der Modellierung eigener Daten Neuland betrat, suchte er nach \textit{Best Practice}-Ansätzen, um sich einerseits für das eigene Projekt inspirieren zu lassen und um von den Riesen, auf deren Schultern wir stehen\autocite[Vgl.][]{WP:Zwerge}, eventuell etwas lernen zu können. – Leider wurde er nur sehr spärlich fündig und so geriet er in eine recht pippilangstrumpfhafte „Ich mache mir die Welt, wie sie mir gefällt“-Haltung. Diese war bestimmt vom Handeln nach bestem Wissen und Gewissen und führte zu den folgenden Konventionen für die RDF-Modellierung der SemRes-Daten.
\begin{enumerate}
    \item \label{anchor:konv1} Die RDF-Daten sollten so einfach wie möglich gehalten sein.
    \item \label{anchor:konv2} Innerhalb der verschiedenen RDF-Daten sollten die CRM-Elemente so konsistent wie möglich verwendet werden.
    \item \label{anchor:konv3} Da das CIDOC-CRM mit den Kommentaren zu den Elementen eine gewisse Hilfestellung für die Anwendung des Schemas gibt, sollten diese weitgehend berücksicht/beherzigt werden.
    \item \label{anchor:konv4} Eine Selbstverständlichkeit stellte die korrekte Verwendung der jeweiligen CRM-Klassen und CRM-Eigenschaften innerhalb ihrer definierten \textit{Ranges} und \textit{Domains} dar.
    \item \label{anchor:konv5} Trotz der Einfachheit sollte bei der Modellierung auf das CRM-Konzept der Ereignisse Wert gelegt werden.
    \item \label{anchor:konv6} Es sollte auf \textit{Empty Nodes} verzichtet werden.
    \item \label{anchor:konv7} Alle RDF-Objekte sollten auch anhand einer Bezeichnung identifizierbar sein.
    \begin{itemize}
        \item {\color{DarkSkyBlue}\lstinline+rdfs:label+} sollte bei allen Nodes zur Abbildung der Bezeichner dienen.
        \item Nodes, die in erster Linie nur eine funktionale Rolle spielen, da sie zur Instanzierung benutzt werden – also mittels der CRM-Prädikate {\color{ScarletRed}\lstinline+P???_is_identified_by+} mit ihren Klassenentsprechungen {\color{DarkChameleon}\lstinline+EXX_Appellation+} verbunden sind –, sollten im Bezeichner einen Zusatz ihrer CRM-Klasse aufweisen.
    \end{itemize}
    \item \label{anchor:konv8} Es sollte auf die Verwendung der zusätzlichen, also inversen, Prädikate verzichtet werden, wenn bei der der Modellierung bereits eine Richtung des Graphen definiert wurde.
\end{enumerate}

\minisec{URLs und URIs}
Zu den bereits bestehenden Konventionen kommt noch eine weitere konzeptionelle Festlegung, die die zu verwendenden URIs betrifft. Da sich, wie bereits in der Einleitung auf Seite \pageref{anchor:semres-domain} erwähnt, unter der geplanten Domain \url{http://semres.fh-potsdam.de} das Web-Frontend der SemRes-Anwendung befindet und die URIs Web-Resourcen sein sollen, wurde für das URI-Schema die Erweiterung der URL um den Pfad „database“ gewählt – \texttt{http://semres.fh-potsdam.de/database/}.

Dieser Pfad wird weiterhin um die Bezeichnung der zweiten Ebene des CRM-Schemas erweitert, um eine einfache Unterscheidung der Resourcen anhand ihrer URI treffen zu können. Die folgende Tabelle \ref{tab:uri_paths} listet alle diese Klassen auf und weist ihnen den entsprechenden Pfad zu.
\begin{table}[H]
\footnotesize
\rowcolors{1}{Aluminium1}{white}
\caption{CRM-Klassen und Pfade}
\label{tab:uri_paths}
\begin{tabularx}{\textwidth}{lX}
\toprule
\textbf{CRM-Klasse} & \textbf{Pfad} \\
\midrule
\midrule
E2 Temporal Entity & \texttt{http://semres.fh-potsdam.de/database/temporal/} \\
E77 Persistent Item & \texttt{http://semres.fh-potsdam.de/database/persistent/} \\
E52 Time-Span & \texttt{http://semres.fh-potsdam.de/database/time/} \\
E53 Place & \texttt{http://semres.fh-potsdam.de/database/place/} \\
E54 Dimension & \texttt{http://semres.fh-potsdam.de/database/dimension/} \\
\bottomrule
\end{tabularx}
\end{table}

An das Ende der so entstandenen URI wird schließlich eine ID angehängt, die die jeweilige Resource eindeutig identifiziert. Somit besitzt also beispielsweise die erste SemRes-Resource, die mit der Klasse {\color{DarkChameleon}\lstinline+E53_Place+} definiert ist, die URI {\color{Chocolate}\lstinline+http://semres.fh-potsdam.de/database/place/1+}.

Da es sich bei der gesamten Modellierung um ein eher theoretisches und manuell erstelltes Beispiel handelt, ist im Folgenden der konzeptionelle Charakter zu beachten. Das heißt, dass die verwendeten URIs und IDs im Sinne des Semantic Web nur auf eine grundlegende funktionale Korrektheit, aber nicht auf das erfolgreiche Zusammenspiel mit der zugrundeliegenden Datenbank beziehungsweise einem nicht existierenden Triplestore geprüft werden konnten.

\section{Verwendete CRM-Elemente}
Um im Folgekapitel nicht den Überblick zu verlieren werden an dieser Stelle alle zur Modellierung benötigten CRM-Elemente mit einem Link auf ihre zugehörige Definition in der bereits erwähnten offiziellen CIDOC-CRM-Dokumentation unter [0]\footnote{[0] \url{http://www.cidoc-crm.org/html/5.0.4/cidoc-crm.html}} aufgelistet.

Tabelle \ref{tab:used_classes} listet alle verwendeten CRM-Klassen, sortiert nach ihrem Namen (inklusive der Nummer), auf.
\begin{table}[H]
\footnotesize
\rowcolors{1}{Aluminium1}{white}
\caption{Liste aller zur Modellierung verwendeten CRM-Klassen}
\label{tab:used_classes}
\begin{tabularx}{\textwidth}{lX}
\toprule
\textbf{CRM-Klasse} & \textbf{HTML-Dokumentation} \\
\midrule
\midrule
\texttt{E11\_Modification} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E11_Modification}} \\
\texttt{E12\_Production} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E12_Production}} \\
\texttt{E31\_Document} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E31_Document}} \\
\texttt{E39\_Actor} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E39_Actor}} \\
\texttt{E57\_Material} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E57_Material}} \\
\texttt{E61\_Time\_Primitive} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E61_Time_Primitive}} \\
\texttt{E74\_Group} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E74_Group}} \\
\texttt{E82\_Actor\_Appellation} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E82_Actor_Appellation}} \\
\texttt{E84\_Information\_Carrier} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/E84_Information_Carrier}} \\
\bottomrule
\end{tabularx}
\end{table}

In Tabelle \ref{tab:used_properties} werden analog zu der vorangegangenen Tabelle alle verwendeten CRM-Eigenschaften aufgelistet.
\begin{table}[H]
\footnotesize
\rowcolors{1}{Aluminium1}{white}
\caption{Liste aller zur Modellierung verwendeten CRM-Eigenschaften}
\label{tab:used_properties}
\begin{tabularx}{\textwidth}{lX}
\toprule
\textbf{CRM-Eigenschaft} & \textbf{Pfad} \\
\midrule
\midrule
\texttt{P3\_has\_note} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P3_has_note}} \\
\texttt{P14\_carried\_out\_by} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by}} \\
\texttt{P31i\_was\_modified\_by} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P31i_was_modified_by}} \\
\texttt{P45\_consists\_of} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P45_consists_of}} \\
\texttt{P70i\_is\_documented\_in} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P70i_is_documented_in}} \\
\texttt{P72\_has\_language} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P72_has_language}} \\
\texttt{P82\_at\_some\_time\_within} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P82_at_some_time_within}} \\
\texttt{P108i\_was\_produced\_by} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P108i_was_produced_by}} \\
\texttt{P127i\_has\_narrower\_term} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P127i_has_narrower_term}} \\
\texttt{P131\_is\_identified\_by} & {\scriptsize \url{http://www.cidoc-crm.org/cidoc-crm/P131_is_identified_by}} \\
\bottomrule
\end{tabularx}
\end{table}

